#!/usr/bin/env tdaq_python

from datetime import datetime
import unittest

from coca_mda_admin.ui.time_edit import _re_time_str, _re_delta_str, _parse_time, _apply_delta, combine


class TestCase(unittest.TestCase):

    def test_re_delta(self) -> None:

        no_match = [
            "", " ", "    ",
            "-", " -", "- ", "  -  ",
            "+", " +", "+ ", "  +  ",
            "+ y", " + d", "+ day", "day",
            "+ 1 day 5 years",
        ]
        for entry in no_match:
            m = _re_delta_str.match(entry)
            self.assertIsNone(m)

        matches = [
            "+ 1y", "+1 Y", " + 121 year", "+15years",
            "+ 1mo", "+1 Mo", " + 121 month", "+15months",
            "+ 1d", "+1 D", " + 121 day", "+15days",
            "+ 1h", "+1 H", " + 121 hour", "+15hours",
            "+ 1m", "+1 Min", " + 121 minute", "+15minutes",
            "+ 1s", "+1 Sec", " + 121 second", "+15seconds",
            "+1y1mo24d5h20m32s",
            "+ 5h20m32s",
            "+ 1 year 24 days",
        ]
        for entry in matches:
            m = _re_delta_str.match(entry)
            self.assertIsNotNone(m)
            m = _re_delta_str.match(entry.replace("+", "-"))
            self.assertIsNotNone(m)

    def test_re_absolute(self) -> None:

        no_match = [
            "", " ", "    ",
            "2001", "2001-11", "2001-01-01T10", "2001-01-01 00",
            "now too", "now + ", "now - ", "now + d",
        ]
        for entry in no_match:
            m = _re_time_str.match(entry)
            self.assertIsNone(m)

        matches = [
            "now", "NOW", " today ", " tomorrow", "Yesterday ",
            "2012-12-31", "2013-01-01 00:00", "2013-01-01T00:00",
            " 2012-12-31 10:11:12 ", " 2012-12-31T10:11:12",
            "now + 1d", "today - 3 mo", "yesterday + 12 seconds",
        ]
        for entry in matches:
            m = _re_time_str.match(entry)
            self.assertIsNotNone(m)
            m = _re_time_str.match(entry.replace("+", "-"))
            self.assertIsNotNone(m)

    def test_parse(self) -> None:

        a, d = _parse_time("")
        self.assertIsNone(a)
        self.assertIsNone(d)

        with self.assertRaises(ValueError):
            _parse_time("not now")

        for tstr in ["now", "today", "yesterday", "tomorrow"]:
            a, d = _parse_time(tstr)
            self.assertIsNotNone(a)
            self.assertIsNone(d)

        for tstr in ["+ 1d", "- 12 sec", "+ 1y2mo"]:
            a, d = _parse_time(tstr)
            self.assertIsNone(a)
            self.assertIsNotNone(d)

        for tstr in ["2020-01-01+1d", "2020-02-02-12sec", "now + 1y2mo"]:
            a, d = _parse_time(tstr)
            self.assertIsNotNone(a)
            self.assertIsNotNone(d)

        a, d = _parse_time("2020-01-02")
        self.assertIsNone(d)
        self.assertEqual(a, (2020, 1, 2, 0, 0, 0))

        a, d = _parse_time("2020-01-01 10:11:55")
        self.assertIsNone(d)
        self.assertEqual(a, (2020, 1, 1, 10, 11, 55))

        a, d = _parse_time("+1y")
        self.assertIsNone(a)
        self.assertEqual(d, (1, 0, 0, 0, 0, 0))

        a, d = _parse_time("-1y2mo3d")
        self.assertIsNone(a)
        self.assertEqual(d, (-1, -2, -3, 0, 0, 0))

        a, d = _parse_time("-1y2h4m5sec")
        self.assertIsNone(a)
        self.assertEqual(d, (-1, 0, 0, -2, -4, -5))

        a, d = _parse_time("2020-01-02 + 32d 5h")
        self.assertEqual(a, (2020, 1, 2, 0, 0, 0))
        self.assertEqual(d, (0, 0, 32, 5, 0, 0))

    def test_apply_delta(self) -> None:

        absolute = (2020, 10, 31, 12, 13, 55)

        dt = _apply_delta(absolute, None)
        self.assertIsNone(dt.tzinfo)
        self.assertEqual(dt.isoformat(), "2020-10-31T12:13:55")

        dt = _apply_delta(absolute, (10, 0, 0, 0, 0, 0))
        self.assertEqual(dt.isoformat(), "2030-10-31T12:13:55")

        dt = _apply_delta(absolute, (-10, 0, 0, 0, 0, 0))
        self.assertEqual(dt.isoformat(), "2010-10-31T12:13:55")

        dt = _apply_delta(absolute, (0, 2, 0, 0, 0, 0))
        self.assertEqual(dt.isoformat(), "2020-12-31T12:13:55")

        dt = _apply_delta(absolute, (0, -2, 0, 0, 0, 0))
        self.assertEqual(dt.isoformat(), "2020-08-31T12:13:55")

        # Last day of month
        dt = _apply_delta(absolute, (0, 1, 0, 0, 0, 0))
        self.assertEqual(dt.isoformat(), "2020-11-30T12:13:55")

        # Last day of month
        dt = _apply_delta(absolute, (0, 4, 0, 0, 0, 0))
        self.assertEqual(dt.isoformat(), "2021-02-28T12:13:55")

        dt = _apply_delta(absolute, (0, 0, 1, 0, 0, 0))
        self.assertEqual(dt.isoformat(), "2020-11-01T12:13:55")

        dt = _apply_delta(absolute, (0, 0, -1, 2, 0, 0))
        self.assertEqual(dt.isoformat(), "2020-10-30T14:13:55")

        dt = _apply_delta(absolute, (0, 0, 0, 0, 10, 10))
        self.assertEqual(dt.isoformat(), "2020-10-31T12:24:05")

        dt = _apply_delta(absolute, (0, 0, 0, 0, -10, -10))
        self.assertEqual(dt.isoformat(), "2020-10-31T12:03:45")

    def test_combine(self) -> None:

        now = datetime.utcnow()
        epoch = datetime.utcfromtimestamp(0)
        now_nsec = int((now - epoch).total_seconds()) * 1_000_000_000

        with self.assertRaises(ValueError):
            combine("", "")

        since, until = combine("now", "", now)
        self.assertEqual(since, until)
        self.assertEqual(since, now_nsec)
        self.assertEqual(until, now_nsec)

        since, until = combine("", "now", now)
        self.assertEqual(since, until)
        self.assertEqual(since, now_nsec)
        self.assertEqual(until, now_nsec)

        since, until = combine("- 1h", "", now)
        self.assertEqual(since, now_nsec - 3600 * 1_000_000_000)
        self.assertEqual(until, now_nsec)

        since, until = combine("", " + 1h ", now)
        self.assertEqual(since, now_nsec)
        self.assertEqual(until, now_nsec + 3600 * 1_000_000_000)

        since, until = combine(" - 1h ", " + 1h ", now)
        self.assertEqual(since, now_nsec - 3600 * 1_000_000_000)
        self.assertEqual(until, now_nsec + 3600 * 1_000_000_000)

        dt = datetime(2020, 1, 1)
        dt_nsec = int((dt - epoch).total_seconds()) * 1_000_000_000

        since, until = combine("2020-01-01", " + 1h ")
        self.assertEqual(since, dt_nsec)
        self.assertEqual(until, dt_nsec + 3600 * 1_000_000_000)

        since, until = combine("-1h", "2020-01-01")
        self.assertEqual(since, dt_nsec - 3600 * 1_000_000_000)
        self.assertEqual(until, dt_nsec)

        since, until = combine("2020-01-01 + 2h", " + 1h ")
        self.assertEqual(since, dt_nsec + 2 * 3600 * 1_000_000_000)
        self.assertEqual(until, dt_nsec + 3 * 3600 * 1_000_000_000)

        since, until = combine("-1h", "2020-01-01 - 2h")
        self.assertEqual(since, dt_nsec - 3 * 3600 * 1_000_000_000)
        self.assertEqual(until, dt_nsec - 2 * 3600 * 1_000_000_000)

        since, until = combine("2020-01-01 - 1h", "2020-01-01 + 1h")
        self.assertEqual(since, dt_nsec - 3600 * 1_000_000_000)
        self.assertEqual(until, dt_nsec + 3600 * 1_000_000_000)


if __name__ == "__main__":
    unittest.main()
