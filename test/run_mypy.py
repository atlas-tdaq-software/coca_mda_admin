#!/usr/bin/env tdaq_python

#
# Wrapper script to run mypy on sources if mypy can be found
#

import sys

try:
    from mypy.__main__ import console_entry
except ImportError:
    # use special return code which signals skipping to ctest
    print("Failed to import mypy, test skipped")
    sys.exit(66)

console_entry()
