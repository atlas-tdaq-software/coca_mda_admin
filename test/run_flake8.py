#!/usr/bin/env tdaq_python

#
# Wrapper script to run flake8 on sources if flake8 can be found
#

import sys

try:
    from flake8.main import cli
except ImportError:
    # use special return code which signals skipping to ctest
    print("Failed to import flake8, test skipped")
    sys.exit(66)

sys.exit(cli.main())
