"""Helper methods and classes for UI part."""

from __future__ import annotations

__all__ = ["UISelectableText"]


import logging
from typing import Any

import urwid

_log = logging.getLogger(__name__)


class UISelectableText(urwid.Text):
    """Text widget supporting selection.

    Parameters
    ----------
    markup
        Text Markup: content of text widget.
    align : `str`
        Text alignment mode, typically 'left', 'center' or 'right'.
    wrap : `str`
        Text wrapping mode, typically 'space', 'any' or 'clip'.
    layout : `urwid.TextLayout`, optional
        Text layout instance, defaults to a shared StandardTextLayout instance.
    """

    _selectable = True

    def __init__(
        self, markup: Any, align: str = "left", wrap: str = "space", layout: urwid.TextLayout | None = None
    ):
        urwid.Text.__init__(self, markup, align, wrap, layout)

    def keypress(self, size: tuple, key: str) -> str | None:
        return key
