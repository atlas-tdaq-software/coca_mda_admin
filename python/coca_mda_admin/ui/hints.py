"""Helper methods and classes for UI part."""

from __future__ import annotations

__all__ = ["UIHints"]

import urwid


class UIHints(urwid.WidgetWrap):
    """Widget for a line of text which displays "hints" which are typically
    keyboard shortcuts.
    """

    def __init__(self, attr: str = "hint", attr_high: str = "hint-hight"):
        self._attr = attr
        self._attr_high = attr_high
        self._hints = urwid.Text("", wrap="clip")
        super().__init__(urwid.AttrMap(self._hints, self._attr))

        self._hint_stack: list[str | list] = []

    def push_hints(self, hints: list[tuple[str, str]]) -> None:
        """Push one more level of hints."""
        self._hint_stack.append("")
        self.set_hints(hints)

    def set_hints(self, hints: list[tuple[str, str]]) -> None:
        """Set hint string (e.g. "^Q: Exit")"""
        message = []
        for key, action in hints:
            message += [" ", ("hint-high", key), ":" + action]
        if self._hint_stack:
            self._hint_stack[-1] = message
        self._hints.set_text(message or "")

    def pop_hints(self) -> None:
        """Restore previous level of hints"""
        if self._hint_stack:
            del self._hint_stack[-1]
        message = self._hint_stack[-1] if self._hint_stack else ""
        self._hints.set_text(message)
