"""Helper methods and classes for UI part."""

from __future__ import annotations

__all__ = ["UIMenu", "UIPopUpMenu"]

import logging
from collections.abc import Callable
from typing import Any

import urwid

from .popup import UIPopUp

_log = logging.getLogger(__name__)


class UIMenu(urwid.WidgetWrap):
    """
    Widget implementing simple menu.

    Consists of a ListBox with a bunch of buttons, when button
    is clicked this widget generates 'selected' signal with a value
    which corresponds to that particular button. Optionally Escape
    key can be configured to also generate 'selected' signal with
    specified value.

    Parameters
    ----------
    title : `str`
        Menu title
    options : `list`
        List of tuples (string, value)
    escape_value
        If not None then escape key will generate 'selected' signal with this
        value.
    """

    signals = ["selected"]

    def __init__(self, title: str, options: list[tuple[str, Any]], escape_value: Any = None):
        items = []
        for name, value in options:
            button = urwid.Button(name)
            urwid.connect_signal(button, "click", self._callback, user_args=[value])
            items.append(urwid.AttrMap(button, None, "menu-focus"))
        listbox = urwid.ListBox(urwid.SimpleFocusListWalker(items))
        linebox = urwid.LineBox(listbox, title)
        self.__super.__init__(urwid.AttrMap(linebox, "menu"))

        self.escape_value = escape_value

    def _callback(self, value: Any, button: urwid.Button) -> None:
        self._emit("selected", value)

    def keypress(self, size: tuple, key: str) -> str | None:
        _log.debug("UIMenu: received key: %s", key)
        if self.escape_value is not None and key == "esc":
            _log.debug("UIMenu: escape key: %s", self.escape_value)
            self._emit("selected", self.escape_value)
            return None
        return self.__super.keypress(size, key)


class UIPopUpMenu(UIPopUp):
    """Pop-up showing a menu.

    Parameters
    ----------
    callback : `~collections.abc.Callable`
        Method to call when menu item is selected.
    cols : `int`
        Width of a pop-up window.

    Other parameters are same as for `UIMessageBox` class.
    """

    def __init__(
        self,
        title: str,
        options: list[tuple[str, Any]],
        callback: Callable,
        escape_value: Any = None,
        cols: int = 24,
    ):
        menu = UIMenu(title, options, "cancel")
        # NOTE: the order of callbacks is important
        urwid.connect_signal(menu, "selected", self.close)
        urwid.connect_signal(menu, "selected", callback)

        pop_up_size = (cols, len(options) + 2)

        self.__super.__init__(menu, pop_up_size)

    def hints(self) -> list[tuple[str, str]]:
        return [("Esc", "Cancel"), ("Enter", "Select")]
