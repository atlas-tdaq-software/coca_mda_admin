"""Helper methods and classes for UI part."""

from __future__ import annotations

__all__ = ["UIMessageBox", "UIPopUpMessageBox"]

import logging
from collections.abc import Callable
from typing import Any

import urwid

from .popup import UIPopUp

_log = logging.getLogger(__name__)


class UIMessageBox(urwid.WidgetWrap):
    """Widget implementing simple message box.

    Consists of a text widget with a message and few buttons. When
    button is clicked this widget generates 'selected' signal with a
    value which corresponds to that particular button. Optionally Escape
    key can be configured to also generate 'selected' signal with
    specified value.

    Parameters
    ----------
    title : `str`
        Widget title.
    message :  `str`
        Message string.
    buttons : `list`
        List of ('button_text', value) tuples, value is used for 'selected'
        signal when corresponding button is clicked.
    escape_value
        If not None then escape key will generate 'selected' signal with this
        value.
    """

    signals = ["selected"]

    def __init__(
        self,
        title: str,
        message: str,
        buttons: list[tuple[str, Any]],
        escape_value: Any = None,
        attr: str = "messagebox",
    ):
        stretch = urwid.Text(" " * 132, wrap="clip")  # stretch space to the left of the buttons
        items = [stretch]
        for name, value in buttons:
            button = urwid.Button(name)
            urwid.connect_signal(button, "click", self._callback, user_args=[value])
            size = len(name) + 4
            items.append((size, urwid.AttrMap(button, None, attr + "-focus")))
        items += [stretch]
        button_row = urwid.Columns(items, 2)
        text = urwid.Text(message)
        pile = urwid.Pile([text, urwid.Divider("\u2500"), button_row])
        filler = urwid.Filler(pile, height="flow")  # need to wrap it into a box widget
        linebox = urwid.LineBox(filler, title)  # wrap it all into linebox with title
        super().__init__(urwid.AttrMap(linebox, attr))

        self.escape_value = escape_value

    def _callback(self, value: Any, button: urwid.Button) -> None:
        self._emit("selected", value)

    def keypress(self, size: tuple, key: str) -> str | None:
        _log.debug("UIMenu: received key: %s", key)
        if self.escape_value is not None and key == "esc":
            _log.debug("UIMenu: escape key: %s", self.escape_value)
            self._emit("selected", self.escape_value)
            return None
        return super().keypress(size, key)


class UIPopUpMessageBox(UIPopUp):
    """Pop-up showing a message box.

    Parameters
    ----------
    callback : `~collections.abc.Callable`, optional
        Method to call when any button is activated.
    cols : `int`
        Width of a pop-up window.

    Other parameters are the same as for `UIMenu` class.
    """

    def __init__(
        self,
        title: str,
        message: str,
        buttons: list[urwid.Button],
        callback: Callable | None = None,
        escape_value: Any = None,
        attr: str = "messagebox",
        cols: int = 48,
    ):
        mbox = UIMessageBox(title, message, buttons, escape_value, attr)
        # NOTE: the order of callbacks is important
        urwid.connect_signal(mbox, "selected", self.close)
        if callback is not None:
            urwid.connect_signal(mbox, "selected", callback)

        rows = urwid.Text(message).rows((cols,))
        pop_up_size = (cols + 2, rows + 4)

        super().__init__(mbox, pop_up_size)

    def hints(self) -> list[tuple[str, str]]:
        return [("Esc", "Cancel")]
