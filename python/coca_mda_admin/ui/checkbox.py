"""Helper methods and classes for UI part."""

from __future__ import annotations

__all__ = ["SlimCheckBox"]

import urwid


def _keymap_for_checkbox() -> dict[str, str | None]:
    # Do not use enter for toggle, only space and insert
    command_map: dict[str, str | None] = urwid.command_map.copy()
    command_map["enter"] = None
    command_map["insert"] = "activate"
    return command_map


class SlimCheckBox(urwid.CheckBox):
    """CheckBox which is one character wide."""

    states = {
        True: urwid.SelectableIcon(("selected", "X")),
        False: urwid.SelectableIcon(" "),
        "mixed": urwid.SelectableIcon("#"),
    }
    reserve_columns = 1

    _command_map = _keymap_for_checkbox()
