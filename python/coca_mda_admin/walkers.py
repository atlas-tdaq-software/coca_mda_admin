"""Helper methods and classes for listbox walkers.
"""
from __future__ import annotations

import logging
from collections.abc import Callable, Iterable
from typing import Any

import urwid

_log = logging.getLogger(__name__)


class PageWalker(urwid.ListWalker):
    """Special walker class with pagination support."""

    def __init__(self, pager: Callable[[int, int], list], count: int, pageSize: int = 512, nPages: int = 10):
        """
        @param pager:     callable which takes two parametes (offset, count)
                          and returns list of items/widgets (or empty list)
        @param count:     total number of items
        @param pageSize:  number of items to fetch at once
        @param nPages:    number of pages to keep in MRU list
        """
        self._pager = pager
        self._count = count
        self._pagesize = pageSize
        self._npages = nPages
        self.focus = 0

        self._pages: list[tuple[int, list]] = []  # list of (page_number, [items])

    def reset(self, new_count: int) -> None:
        self._count = new_count
        # reset page cache
        self._pages = []

        # if focus is beyond the last item move focus
        if self.focus >= self._count:
            self.focus = self._count - 1

        # notify everyone
        self._modified()

    def _get_page(self, page: int) -> list:
        # search in MRU pages first:
        for i, (p, items) in enumerate(self._pages):
            if p == page:
                _log.debug("walker: found in MRU at %s", i)
                # MRU to front
                if i != 0:
                    del self._pages[i]
                    self._pages.insert(0, (p, items))
                return items

        _log.debug("walker: fetching page %s", page)
        items = self._pager(page * self._pagesize, self._pagesize)
        _log.debug("walker: fetched %s items", len(items))

        # add to MRU
        self._pages.insert(0, (page, items))
        if len(self._pages) > self._npages:
            del self._pages[-1]
        _log.debug("walker: MRU size %s", len(self._pages))
        return items

    def _get_item(self, position: int) -> tuple[Any, int] | tuple[None, None]:
        _log.debug("walker: _get_item %s", position)
        if not 0 <= position < self._count:
            _log.debug("walker: out of range, return None")
            return None, None

        page, offset = divmod(position, self._pagesize)
        _log.debug("walker: page/offset: %s %s", page, offset)
        items = self._get_page(page)
        if offset >= len(items):
            _log.debug("walker: page too short, return None")
            return None, None

        return items[offset], position

    def get_focus(self) -> tuple[Any, int] | tuple[None, None]:
        _log.debug("walker: get_focus %s", self.focus)
        return self._get_item(self.focus)

    def get_next(self, position: int) -> tuple[Any, int] | tuple[None, None]:
        _log.debug("walker: get_next %s", position)
        return self._get_item(position + 1)

    def get_prev(self, position: int) -> tuple[Any, int] | tuple[None, None]:
        _log.debug("walker: get_prev %s", position)
        return self._get_item(position - 1)

    def set_focus(self, focus: int) -> None:
        _log.debug("walker: set_focus %s", focus)
        self.focus = focus

    def positions(self, reverse: bool = False) -> Iterable[int]:
        if not reverse:
            return range(self._count)
        else:
            return range(self._count - 1, -1, -1)
