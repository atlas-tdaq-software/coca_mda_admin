"""Module containing CocaDb class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
from contextlib import contextmanager
from datetime import datetime
import logging
from collections.abc import Iterable, Iterator
from typing import NamedTuple, TypeVar, cast

#-----------------------------
# Imports for other modules --
#-----------------------------
from coldpie import coral

#----------------------------------
# Local non-exported definitions --
#----------------------------------
_log = logging.getLogger(__name__)


_NamedTupleType = TypeVar("_NamedTupleType", bound=NamedTuple)


def _ts2dt(ts: coral.TimeStamp) -> datetime:
    """Convert Coral timestamp into datetime"""
    return datetime(
        ts.year(), ts.month(), ts.day(), ts.hour(), ts.minute(), ts.second(), ts.nanosecond() // 1000
    )


def _row2nt(row: coral.AttributeList, tupletype: type[_NamedTupleType]) -> _NamedTupleType:
    """Convert Coral AttributeList (result row) into a named tuple."""
    kv = {}
    for attr in tupletype._fields:
        val = row[attr].data()
        if isinstance(val, coral.TimeStamp):
            val = _ts2dt(val)
        kv[attr] = val
    return tupletype(**kv)  # type: ignore[call-overload]


@contextmanager
def read_transaction(session: coral.ISessionProxy) -> Iterator:
    readonly = True
    trans = session.transaction()
    trans.start(readonly)
    yield trans
    trans.commit()


@contextmanager
def update_transaction(session: coral.ISessionProxy) -> Iterator:
    readonly = False
    trans = session.transaction()
    trans.start(readonly)
    yield trans
    trans.commit()

#------------------------
# Exported definitions --
#------------------------


class Server(NamedTuple):
    """Information about a coca server."""
    server_ipc: str
    """Server IPC name."""

    n_archives: int
    """Total archive count."""

    totsize_bytes: int
    """Total archive size im Bytes."""


class Dataset(NamedTuple):
    name: str
    n_archives: int
    totsize_bytes: int


class Archive(NamedTuple):
    archive: int
    name: str
    dataset: int
    dataset_name: str
    booked_at: datetime
    close_at: datetime
    delete_at: datetime
    server_ipc: int
    server_ipc_name: str
    server_host: str
    dir_server: str
    totsize_bytes: int
    archive_size_bytes: int
    closed_at: datetime
    on_cdr: bool
    on_cdr_at: datetime
    cdr_path: str
    stored: bool
    stored_at: datetime
    stor_path: str
    removed: bool
    removed_at: datetime
    open_if_0: int


class File(NamedTuple):
    dataset: int
    dataset_name: str
    rel_path: str
    size_bytes: int
    client_host: str
    dir_client: str
    prio: int
    try2keep_til: datetime
    registered_at: datetime
    archive: int
    archive_name: str
    on_client: bool
    in_cache: bool
    in_cache_at: datetime
    removed: bool
    removed_at: datetime


class CocaDb:
    """Interface for CoCa database.
    """

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, conn_str: str, readonly: bool):
        """
        @param conn_str:  CORAL connection string for coca database
        @param readonly:  boolean, if True then use read-only mode for database
        """

        conn_svc = coral.ConnectionService()
        mode = coral.access_ReadOnly if readonly else coral.access_Update
        logging.debug("start new database session: %s", conn_str)
        self.session = conn_svc.connect(conn_str, mode)
        self.schema = self.session.nominalSchema()

        self.s2iName = 'COCA_STR2ID'
        self.arName = 'COCA1_ARCHIVES'
        self.caName = 'COCA1_CACHE'

    def servers(self) -> list[Server]:
        """
        Returns the list of Server instances ordered by IPC server_ipc name.
        """

        with read_transaction(self.session):

            query = self.schema.newQuery()

            query.addToTableList(self.arName, "AR")
            query.addToTableList(self.s2iName, "S2I_IPC")
            query.addToOutputList('S2I_IPC.STR', 'server_ipc')
            query.addToOutputList('COUNT(*)', 'n_archives')
            query.defineOutputType('n_archives', 'long')
            query.addToOutputList('SUM(COALESCE(AR.ARCHIVE_SIZE_BYTES, 0))', 'totsize_bytes')
            query.defineOutputType('totsize_bytes', 'long')

            cdata = coral.AttributeList()
            cond = """AR.SERVER_IPC = S2I_IPC.ID"""
            query.setCondition(cond, cdata)

            query.addToOrderList('S2I_IPC.STR')
            query.groupBy('S2I_IPC.STR')

            res = [_row2nt(row, Server) for row in query.execute()]

        return res

    def datasets(self, server: str | None) -> list[Dataset]:
        """
        Returns list of Dataset instances ordered by name.

        @param server: server name or None, if None returns datasets for all servers.
        """

        with read_transaction(self.session):

            query = self.schema.newQuery()
            query.addToTableList(self.arName, "AR")
            query.addToTableList(self.s2iName, "S2I_DS")

            query.addToOutputList('S2I_DS.STR', 'name')
            query.addToOutputList('COUNT(*)', 'n_archives')
            query.defineOutputType('n_archives', 'long')
            query.addToOutputList('SUM(COALESCE(AR.ARCHIVE_SIZE_BYTES, 0))', 'totsize_bytes')
            query.defineOutputType('totsize_bytes', 'long')

            cdata = coral.AttributeList()
            cond = """AR.DATASET = S2I_DS.ID"""
            if server:
                query.addToTableList(self.s2iName, "S2I_IPC")
                cdata.extend('IPCNAME', 'string')
                cdata[0].setData(server)
                cond += """ AND AR.SERVER_IPC = S2I_IPC.ID AND S2I_IPC.STR = :IPCNAME"""
            query.setCondition(cond, cdata)

            query.addToOrderList('S2I_DS.STR')
            query.groupBy('S2I_DS.STR')

            res = [_row2nt(row, Dataset) for row in query.execute()]

        return res

    def archives_count(self, server: str | None, dataset: str | None, is_open: bool | None = None,
                       stored: bool | None = None, removed: bool | None = None) -> tuple[int, int]:
        """
        @param server: server name or None
        @param dataset: dataset name or None
        @param is_open: True/False or None
        @param stored: True/False or None
        @param removed: True/False or None
        @return: tuple of archive count and archive size in Bytes
        """
        _log.debug('archives_count: server, dataset, is_open = %r, %r, %r', server, dataset, is_open)

        with read_transaction(self.session):

            query = self.schema.newQuery()
            query.addToTableList(self.arName, "AR")
            query.addToOutputList('COUNT(*)', 'n_archives')
            query.defineOutputType('n_archives', 'long')
            query.addToOutputList('SUM(COALESCE(AR.ARCHIVE_SIZE_BYTES, 0))', 'totsize_bytes')
            query.defineOutputType('totsize_bytes', 'long')

            cdata = coral.AttributeList()
            conditions = []
            if server:
                query.addToTableList(self.s2iName, "S2I_IPC")
                idx = len(cdata)
                cdata.extend('IPCNAME', 'string')
                cdata[idx].setData(server)
                conditions += ["AR.SERVER_IPC = S2I_IPC.ID", "S2I_IPC.STR = :IPCNAME"]
            if dataset:
                query.addToTableList(self.s2iName, "S2I_DS")
                idx = len(cdata)
                cdata.extend('DATASET', 'string')
                cdata[idx].setData(dataset)
                conditions += ["AR.DATASET = S2I_DS.ID", "S2I_DS.STR = :DATASET"]
            if is_open is True:
                conditions += ["AR.OPEN_IF_0 = 0"]
            if is_open is False:
                conditions += ["AR.OPEN_IF_0 <> 0"]
            if stored is True:
                conditions += ["AR.STORED <> 0"]
            if stored is False:
                conditions += ["AR.STORED = 0"]
            if removed is True:
                conditions += ["AR.REMOVED <> 0"]
            if removed is False:
                conditions += ["AR.REMOVED = 0"]
            if conditions:
                cond = " AND ".join(conditions)
                _log.debug('archives_count: cond: %r', cond)
                query.setCondition(cond, cdata)

            counter = 0
            size = 0
            for row in query.execute():

                counter = int(cast(float, row[0].data()))
                size = int(cast(float, row[1].data() or 0.))

        return counter, size

    def archives(self, server: str | None, dataset: str | None, is_open: bool | None = None,
                 stored: bool | None = None, removed: bool | None = None,
                 maxRows: int | None = None, offset: int | None = None) -> list[Archive]:
        """
        Returns list of Archive instances ordered by booking time.

        @param server: server name or None
        @param dataset: dataset name or None
        @param is_open: True/False or None
        @param stored: True/False or None
        @param removed: True/False or None
        @param maxRows: max. number of rows to return (used for paging)
        @param offset: starting position in number of rows (used for paging)
        """

        with read_transaction(self.session):

            query = self.schema.newQuery()

            query.addToTableList(self.arName, "AR")
            query.addToTableList(self.s2iName, "S2I_IPC")
            query.addToTableList(self.s2iName, "S2I_DS")
            query.addToTableList(self.s2iName, "S2I_DIR")
            query.addToTableList(self.s2iName, "S2I_HOST")

            ar_attributes = (
                ('dataset', "long"),
                ('archive', "long"),
                ('name', "string"),
                ('totsize_bytes', "int"),
                ('archive_size_bytes', "int"),
                ('booked_at', "time stamp"),
                ('close_at', "time stamp"),
                ('delete_at', "time stamp"),
                ('server_ipc', "int"),
                ('open_if_0', "int"),
                ('closed_at', "time stamp"),
                ('on_cdr', "bool"),
                ('on_cdr_at', "time stamp"),
                ('cdr_path', "string"),
                ('stored', "bool"),
                ('stored_at', "time stamp"),
                ('stor_path', "string"),
                ('removed', "bool"),
                ('removed_at', "time stamp"),
            )
            for attr, attr_type in ar_attributes:
                query.addToOutputList("AR." + attr.upper(), attr)
                query.defineOutputType(attr, attr_type)
            query.addToOutputList('S2I_DS.STR', 'dataset_name')
            query.defineOutputType("dataset_name", "string")
            query.addToOutputList('S2I_IPC.STR', 'server_ipc_name')
            query.defineOutputType("server_ipc_name", "string")
            query.addToOutputList('S2I_HOST.STR', 'server_host')
            query.defineOutputType("server_host", "string")
            query.addToOutputList('S2I_DIR.STR', 'dir_server')
            query.defineOutputType("dir_server", "string")

            cdata = coral.AttributeList()
            conditions = ["AR.SERVER_IPC = S2I_IPC.ID",
                          "AR.DATASET = S2I_DS.ID",
                          "AR.DIR_SERVER = S2I_DIR.ID",
                          "AR.SERVER_HOST = S2I_HOST.ID"]
            if server:
                idx = len(cdata)
                cdata.extend('IPCNAME', 'string')
                cdata[idx].setData(server)
                conditions += ["S2I_IPC.STR = :IPCNAME"]
            if dataset:
                idx = len(cdata)
                cdata.extend('DATASET', 'string')
                cdata[idx].setData(dataset)
                conditions += ["S2I_DS.STR = :DATASET"]
            if is_open is True:
                conditions += ["AR.OPEN_IF_0 = 0"]
            if is_open is False:
                conditions += ["AR.OPEN_IF_0 <> 0"]
            if stored is True:
                conditions += ["AR.STORED <> 0"]
            if stored is False:
                conditions += ["AR.STORED = 0"]
            if removed is True:
                conditions += ["AR.REMOVED <> 0"]
            if removed is False:
                conditions += ["AR.REMOVED = 0"]

            cond = " AND ".join(conditions)
            query.setCondition(cond, cdata)

            query.addToOrderList('AR.BOOKED_AT')

            if maxRows is not None:
                if offset is None:
                    offset = 0
                query.limitReturnedRows(maxRows, offset)

            query.setRowCacheSize(100000)
            query.setMemoryCacheSize(10)

            res = [_row2nt(row, Archive) for row in query.execute()]

        return res

    def files_in_archive(self, dataset: int, archive: int) -> list[str]:
        """
        @param dataset:  dataset ID (number)
        @param archive:  archive ID (number)
        @return list of file names contained in archive
        """

        with read_transaction(self.session):

            query = self.schema.newQuery()
            query.addToTableList(self.caName, "CA")
            query.addToOutputList('CA.REL_PATH', 'rel_path')

            cdata = coral.AttributeList()
            conditions = []

            idx = len(cdata)
            cdata.extend('DATASET', 'int')
            cdata[idx].setData(dataset)
            idx = len(cdata)
            cdata.extend('ARCHIVE', 'int')
            cdata[idx].setData(archive)
            conditions += ["CA.DATASET = :DATASET", "CA.ARCHIVE = :ARCHIVE"]

            cond = " AND ".join(conditions)
            query.setCondition(cond, cdata)

            query.addToOrderList('CA.REL_PATH')

            res = [cast(str, row[0].data()) for row in query.execute()]

        return res

    def files_count(self, server: str | None, dataset: str | None,
                    on_client: bool | None = None, in_cache: bool | None = None) -> tuple[int, int]:
        """
        @param server: server name or None
        @param dataset: dataset name or None
        @param on_client: True/False or None
        @param in_cache: True/False or None
        @return: tuple of file count and file size (in Bytes)
        """

        with read_transaction(self.session):

            query = self.schema.newQuery()
            query.addToTableList(self.caName, "CA")
            query.addToOutputList('COUNT(*)', 'COUNTER')
            query.defineOutputType('COUNTER', 'long')
            query.addToOutputList('SUM(COALESCE(CA.SIZE_BYTES, 0))', 'SIZE')
            query.defineOutputType('SIZE', 'long')

            cdata = coral.AttributeList()
            conditions = []
            if server or dataset:
                query.addToTableList(self.arName, "AR")
                conditions += ["AR.DATASET = CA.DATASET", "AR.ARCHIVE = CA.ARCHIVE"]
            if server:
                query.addToTableList(self.s2iName, "S2I_IPC")
                idx = len(cdata)
                cdata.extend('IPCNAME', 'string')
                cdata[idx].setData(server)
                conditions += ["AR.SERVER_IPC = S2I_IPC.ID", "S2I_IPC.STR = :IPCNAME"]
            if dataset:
                query.addToTableList(self.s2iName, "S2I_DS")
                idx = len(cdata)
                cdata.extend('DATASET', 'string')
                cdata[idx].setData(dataset)
                conditions += ["AR.DATASET = S2I_DS.ID", "S2I_DS.STR = :DATASET"]
            if on_client is True:
                conditions += ["CA.ON_CLIENT <> 0"]
            if on_client is False:
                conditions += ["CA.ON_CLIENT = 0"]
            if in_cache is True:
                conditions += ["CA.IN_CACHE <> 0"]
            if in_cache is False:
                conditions += ["CA.IN_CACHE = 0"]
            if conditions:
                cond = " AND ".join(conditions)
                query.setCondition(cond, cdata)

            counter = 0
            size = 0
            for row in query.execute():

                counter = int(cast(float, row[0].data()))
                size = int(cast(float, row[1].data() or 0))

        return counter, size

    def files(self, server: str | None, dataset: str | None,
              on_client: bool | None = None, in_cache: bool | None = None,
              maxRows: int | None = None, offset: int | None = None) -> list[File]:
        """
        @param server: server name or None
        @param dataset: dataset name or None
        @param on_client: True/False or None
        @param in_cache: True/False or None
        @param maxRows: max. number of rows to return (used for paging)
        @param offset: starting position in number of rows (used for paging)
        """

        with read_transaction(self.session):

            query = self.schema.newQuery()

            query.addToTableList(self.arName, "AR")
            query.addToTableList(self.caName, "CA")
            query.addToTableList(self.s2iName, "S2I_DS")
            query.addToTableList(self.s2iName, "S2I_DIR")
            query.addToTableList(self.s2iName, "S2I_HOST")

            ca_attributes = (
                ('dataset', "long"),
                ('rel_path', "string"),
                ('size_bytes', "double"),
                ('prio', "int"),
                ('try2keep_til', "time stamp"),
                ('registered_at', "time stamp"),
                ('archive', "long"),
                ('on_client', "bool"),
                ('in_cache', "bool"),
                ('in_cache_at', "time stamp"),
                ('removed', "bool"),
                ('removed_at', "time stamp"),
            )
            for attr, attr_type in ca_attributes:
                query.addToOutputList("CA." + attr.upper(), attr)
                query.defineOutputType(attr, attr_type)
            query.addToOutputList('S2I_DS.STR', 'dataset_name')
            query.addToOutputList('S2I_HOST.STR', 'client_host')
            query.addToOutputList('S2I_DIR.STR', 'dir_client')
            query.addToOutputList('AR.NAME', 'archive_name')

            cdata = coral.AttributeList()
            conditions = ["AR.DATASET = CA.DATASET",
                          "AR.ARCHIVE = CA.ARCHIVE",
                          "CA.DATASET = S2I_DS.ID",
                          "S2I_HOST.ID = CA.CLIENT_HOST",
                          "S2I_DIR.ID = CA.DIR_CLIENT"]

            if server:
                query.addToTableList(self.s2iName, "S2I_IPC")
                idx = len(cdata)
                cdata.extend('IPCNAME', 'string')
                cdata[idx].setData(server)
                conditions += ["AR.SERVER_IPC = S2I_IPC.ID", "S2I_IPC.STR = :IPCNAME"]
            if dataset:
                idx = len(cdata)
                cdata.extend('DATASET', 'string')
                cdata[idx].setData(dataset)
                conditions += ["S2I_DS.STR = :DATASET"]
            if on_client is True:
                conditions += ["CA.ON_CLIENT <> 0"]
            if on_client is False:
                conditions += ["CA.ON_CLIENT = 0"]
            if in_cache is True:
                conditions += ["CA.IN_CACHE <> 0"]
            if in_cache is False:
                conditions += ["CA.IN_CACHE = 0"]

            cond = " AND ".join(conditions)
            query.setCondition(cond, cdata)

            query.addToOrderList('CA.REGISTERED_AT')

            if maxRows is not None:
                if offset is None:
                    offset = 0
                query.limitReturnedRows(maxRows, offset)

            query.setRowCacheSize(10000)
            query.setMemoryCacheSize(10)

            res = [_row2nt(row, File) for row in query.execute()]

        return res

    def archives_set_closed(self, archives: Iterable[tuple[int, int, int]]) -> int:
        """
        @param archives: list of tuples (server_ipc, dataset, archive),
                         server_ipc and dataset are IDs (integers), archive
                         is archive number
        """

        setClause = "OPEN_IF_0 = ARCHIVE"
        extraCond = "OPEN_IF_0 = 0"
        return self._archives_update(archives, setClause, extraCond)

    def archives_set_stored(self, archives: Iterable[tuple[int, int, int]]) -> int:
        """
        @param archives: list of tuples (server_ipc, dataset, archive),
                         server_ipc and dataset are IDs (integers), archive
                         is archive number
        """

        # This does not set STOR_PATH, we'll have separate method for that
        # Note: CURRENT_TIMESTAMP may not be very portable.
        setClause = "STORED = 1, STORED_AT = CURRENT_TIMESTAMP"
        extraCond = "STORED = 0"
        return self._archives_update(archives, setClause, extraCond)

    def archives_set_removed(self, archives: Iterable[tuple[int, int, int]]) -> int:
        """
        @param archives: list of tuples (server_ipc, dataset, archive),
                         server_ipc and dataset are IDs (integers), archive
                         is archive number
        """

        # Note: CURRENT_TIMESTAMP may not be very portable.
        setClause = "REMOVED = 1, REMOVED_AT = CURRENT_TIMESTAMP"
        extraCond = "REMOVED = 0"
        return self._archives_update(archives, setClause, extraCond)

    def _archives_update(self, archives: Iterable[tuple[int, int, int]], setClause: str,
                         extraCond: str = "") -> int:
        """
        @param archives: list of tuples (server_ipc, dataset, archive),
                         server_ipc and dataset are IDs (integers), archive
                         is archive number
        @param setClause: string, SET clause for CORAL update query
        @param extraCond: string, optional WHERE condition
        """

        with update_transaction(self.session):

            table = self.schema.tableHandle(self.arName)
            dataEditor = table.dataEditor()

            cond = "SERVER_IPC = :SERVER_IPC AND DATASET = :DATASET AND ARCHIVE = :ARCHIVE"
            if extraCond:
                cond += " AND " + extraCond

            n_updates = 0
            for ar in archives:

                cdata = coral.AttributeList()

                idx = len(cdata)
                cdata.extend('SERVER_IPC', 'int')
                cdata[idx].setData(ar[0])

                idx = len(cdata)
                cdata.extend('DATASET', 'int')
                cdata[idx].setData(ar[1])

                idx = len(cdata)
                cdata.extend('ARCHIVE', 'int')
                cdata[idx].setData(ar[2])

                n_updates += dataEditor.updateRows(setClause, cond, cdata)

        return n_updates

    def files_add_to_cache(self, files: Iterable[tuple[int, str]]) -> int:
        """
        @param files: list of tuples (dataset, rel_path), dataset is ID
                    (integer), rel_path is a string
        """

        setClause = "IN_CACHE = 1, IN_CACHE_AT = CURRENT_TIMESTAMP"
        extraCond = "IN_CACHE = 0"
        return self._files_update(files, setClause, extraCond)

    def files_remove_from_client(self, files: Iterable[tuple[int, str]]) -> int:
        """
        @param files: list of tuples (dataset, rel_path), dataset is ID
                    (integer), rel_path is a string
        """

        setClause = "ON_CLIENT = 0"
        extraCond = "ON_CLIENT = 1"
        return self._files_update(files, setClause, extraCond)

    def files_remove_from_cache(self, files: Iterable[tuple[int, str]]) -> int:
        """
        @param files: list of tuples (dataset, rel_path), dataset is ID
                    (integer), rel_path is a string
        """

        setClause = "IN_CACHE = 0"
        extraCond = "IN_CACHE = 1"
        return self._files_update(files, setClause, extraCond)

    def _files_update(self, files: Iterable[tuple[int, str]], setClause: str, extraCond: str = "") -> int:
        """
        @param files: list of tuples (dataset, rel_path), dataset is ID
                    (integer), rel_path is a string
        @param setClause: string, SET clause for CORAL update query
        @param extraCond: string, optional WHERE condition
        """

        with update_transaction(self.session):

            table = self.schema.tableHandle(self.caName)
            dataEditor = table.dataEditor()

            cond = "DATASET = :DATASET AND REL_PATH = :REL_PATH"
            if extraCond:
                cond += " AND " + extraCond

            n_updates = 0
            for entry in files:

                cdata = coral.AttributeList()

                idx = len(cdata)
                cdata.extend('DATASET', 'int')
                cdata[idx].setData(entry[0])

                idx = len(cdata)
                cdata.extend('REL_PATH', 'string')
                cdata[idx].setData(entry[1])

                n_updates += dataEditor.updateRows(setClause, cond, cdata)

        return n_updates
