"""Module for CocaAdmin class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppBase import AppBase
from .CocaArchiveInfo import CocaArchiveInfo
from .CocaArchiveList import CocaArchiveList
from . import CocaDb
from .CocaDatasetList import CocaDatasetList
from .CocaFileInfo import CocaFileInfo
from .CocaFileList import CocaFileList
from .CocaServerContentSelect import CocaServerContentSelect
from .CocaServerList import CocaServerList

#----------------------------------
# Local non-exported definitions --
#----------------------------------
_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class CocaAdmin(AppBase):
    """Class representing top-level widget for the whole application.

    Parameters
    ----------
    conn_str: `str`
        CORAL connection string for coca database
    readonly: `bool`
        if True then use read-only mode for database
    """
    def __init__(self, conn_str: str, readonly: bool):
        AppBase.__init__(self)
        self.db = CocaDb.CocaDb(conn_str, readonly)

    def title(self) -> str:
        return ' = CoCa database administration = '

    def start(self, loop: urwid.MainLoop, user_data: object) -> None:
        """Make initial panel
        """
        servers = self.makePanel(CocaServerList, self.db)
        urwid.connect_signal(servers, 'selected', self.serverSelected)

    def serverSelected(self, widget: urwid.Widget, serverName: str) -> None:
        _log.debug("selected server: %r", serverName)
        widget = self.makePanel(CocaDatasetList, self.db, serverName)
        urwid.connect_signal(widget, 'selected', self.datasetSelected)

    def datasetSelected(self, widget: urwid.Widget, serverName: str, dataset: str) -> None:
        _log.debug("selected dataset: %r/%r", serverName, dataset)
        widget = self.makePanel(CocaServerContentSelect, self.db, serverName, dataset)
        urwid.connect_signal(widget, 'selected', self.serverContentSelected)

    def serverContentSelected(self, widget: urwid.Widget, serverName: str, dataset: str,
                              contentType: str, options: dict[str, object]) -> None:
        _log.debug("selected server content: %r %r %r %r", serverName, dataset, contentType, options)
        if contentType == 'archives':
            widget = self.makePanel(CocaArchiveList, self.db, serverName, dataset, options)
            urwid.connect_signal(widget, 'selected', self.archiveSelected)
        elif contentType == 'files':
            widget = self.makePanel(CocaFileList, self.db, serverName, dataset, options)
            urwid.connect_signal(widget, 'selected', self.fileSelected)

    def archiveSelected(self, widget: urwid.Widget, archive: str) -> None:
        _log.debug("selected archive: %r", archive)
        self.makePanel(CocaArchiveInfo, self.db, archive)

    def fileSelected(self, widget: urwid.Widget, fileinfo: CocaDb.File) -> None:
        _log.debug("selected file: %r", fileinfo)
        self.makePanel(CocaFileInfo, fileinfo)
