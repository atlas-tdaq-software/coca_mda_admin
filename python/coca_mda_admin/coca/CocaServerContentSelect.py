"""Module containing CocaServerContentSelect class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import Any, TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText

if TYPE_CHECKING:
    from . import CocaAdmin, CocaDb

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class CocaServerContentSelect(UIListBoxWithHeader, AppPanel):
    """Widget class containing list possible options to display for gien coca
    server, the list typically contains "Archives" and "Files".

    app : `CocaAdmin`
    server : `str`
        name of coca server or None to use all servers
    dataset : `str`
        name of coca dataset or None to use all datasets
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CocaAdmin.CocaAdmin, db: CocaDb.CocaDb, server: str, dataset: str):
        self._app = app
        self._server = server
        self._dataset = dataset

        menu: list[tuple[str, tuple[str, dict[str, Any]]]] = [
            ("All archives", ("archives", dict())),
            ("Open archives", ("archives", dict(open=True))),
            ("Closed archives", ("archives", dict(open=False))),
            ("Stored archives", ("archives", dict(stored=True))),
            ("Non-stored archives", ("archives", dict(stored=False))),
            ("Removed archives", ("archives", dict(removed=True))),
            ("Non-removed archives", ("archives", dict(removed=False))),
            ("All files", ("files", dict())),
            ("Files on client", ("files", dict(on_client=True))),
            ("Files not on client", ("files", dict(on_client=False))),
            ("Files in cache", ("files", dict(in_cache=True))),
            ("Files not in cache", ("files", dict(in_cache=False)))]

        col_width = [20, 10, 15]
        header = UIColumns(['Data selection',
                            urwid.Text('N Archives', align='right'),
                            urwid.Text('Size, MB', align='right')], col_width, 2)

        items = []
        for selection, value in menu:
            count, size = 0, 0
            if value[0] == 'archives':
                count, size = db.archives_count(server, dataset, **value[1])
            elif value[0] == 'files':
                count, size = db.files_count(server, dataset, **value[1])
            size_mb = size / 1024. / 1024.
            item = UIColumns([UISelectableText(selection),
                              urwid.Text(str(count), align='right'),
                              urwid.Text(f"{size_mb:.2f}", align='right')],
                             col_width, 2, 0)
            urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[value])
            items.append(item)

        UIListBoxWithHeader.__init__(self, items, header=header)

    def title(self) -> str:
        if self._server is None:
            return "Content for all servers"
        else:
            return "Content for server {!r}".format(self._server)

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def status(self) -> str:
        return "Database information selector"

    def _itemActivated(self, value: tuple[str, dict[str, Any]], item: UIColumns) -> None:
        infoType, options = value
        _log.debug("emitting signal 'selected': %r %r %r %r", self._server, self._dataset, infoType, options)
        self._emit('selected', self._server, self._dataset, infoType, options)
