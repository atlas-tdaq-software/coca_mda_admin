"""Module containing CocaFileInfo class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import datetime
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBox, UIColumns

if TYPE_CHECKING:
    from . import CocaAdmin, CocaDb

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)


def _fmt(val: object) -> str:
    if isinstance(val, datetime.datetime):
        return val.isoformat(' ')
    elif isinstance(val, bool):
        return "Yes" if val else "No"
    else:
        return str(val)

#------------------------
# Exported definitions --
#------------------------


class CocaFileInfo(UIListBox, AppPanel):
    """Widget class info about single coca fileobj.

    Parameters
    ----------
    app : `CocaAdmin`
    fileobj: `CocaDb.File`
    """

    _selectable = True

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CocaAdmin.CocaAdmin, fileobj: CocaDb.File):

        items = [("Relative path:", fileobj.rel_path),
                 ("Dataset name:", fileobj.dataset_name),
                 ("Registered:", fileobj.registered_at),
                 ("Size, Bytes:", f"{fileobj.size_bytes:n}"),
                 ("Client host:", fileobj.client_host),
                 ("Client dir:", fileobj.dir_client),
                 ("Archive name:", fileobj.archive_name),
                 ("Priority:", fileobj.prio),
                 ("Cache until:", fileobj.try2keep_til),
                 ("On client:", fileobj.on_client),
                 ("In cache:", fileobj.in_cache),
                 ("In cache time:", fileobj.in_cache_at),
                 ("Removed:", fileobj.removed),
                 ("Removed time:", fileobj.removed_at)]

        col_width = [15, -1]
        items = [UIColumns([urwid.Text(item[0], align='right'), urwid.Text(_fmt(item[1]))], col_width, 1)
                 for item in items]

        UIListBox.__init__(self, urwid.SimpleListWalker(items))

    def title(self) -> str:
        return "File information"
