"""Module containing CocaArchiveList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import functools
import logging
from collections.abc import Callable
from typing import Any, TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import (UIListBoxWithHeader, UIColumns, UIPopUpMenu, UIPopUpMessageBox, SlimCheckBox)
from ..walkers import PageWalker

if TYPE_CHECKING:
    from . import CocaAdmin, CocaDb

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)


class ActionsMenu(UIPopUpMenu, AppPanel):

    _actions = [('Cancel', 'cancel'),
                ('Close Archives', 'close'),
                ('Store Archives', 'store'),
                ('Delete Archives', 'delete'),
                ]

    def __init__(self, parent: urwid.Widget):
        self.__super.__init__("Actions", self._actions, parent._confirm_action, 'cancel')


class ConfirmAction(UIPopUpMessageBox):

    def __init__(self, message: str, callback: Callable):
        buttons = [('Yes', 'ok'), ('No', 'cancel')]
        self.__super.__init__('Action confirmation', message, buttons, callback, escape_value='cancel')


class UpdateOK(UIPopUpMessageBox):

    def __init__(self, message: str):
        buttons = [('OK', 'ok')]
        self.__super.__init__('Success', message, buttons, escape_value='ok')


class UpdateFail(UIPopUpMessageBox):

    def __init__(self, message: str):
        buttons = [('Dismiss', 'ok')]
        self.__super.__init__('Failed', message, buttons, escape_value='ok', attr='messagebox-error')

#------------------------
# Exported definitions --
#------------------------


class CocaArchiveList(urwid.WidgetWrap):
    """Widget class containing list of CoCa archives.

    app : `CocaAdmin`
    db : `CocaDb`
    server : `str`
        Name of coca server or None
    dataset : `str`
        Name of coca dataset or None
    options : `dict`
        set of options passed to database API
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CocaAdmin.CocaAdmin, db: CocaDb.CocaDb,
                 server: str, dataset: str, options: dict[str, Any] = {}):
        self._app = app
        self._db = db
        self._server = server
        self._dataset = dataset
        self._checked: set[tuple[int, int, int]] = set()
        self._db_options = options.copy()
        self._col_width = [1, 8, 20, 10, 10, 4, -1]
        self._total_count = self._counter()
        self._listwalker = PageWalker(self._pager, self._total_count)

        header = UIColumns(['X', urwid.Text('ID', align='right'), 'Dataset', 'Created',
                            urwid.Text('Size,MB', align='right'), 'Open', 'Archive'],
                           self._col_width, 1)

        lb = UIListBoxWithHeader(self._listwalker, header=header)
        self.__super.__init__(lb)

    def title(self) -> str:
        if self._server is None:
            return "Archives for all servers"
        else:
            return "Archives for server {!r}".format(self._server)

    def status(self) -> str:
        status = "{} out of {} archives".format(self._listwalker.focus + 1, self._total_count)
        if self._checked:
            status += " ({} checked)".format(len(self._checked))
        return status

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Details"), ('Space', "Select"), ('^E', "Action")]

    def keypress(self, size: tuple[int, int], key: str) -> str | None:

        # handle Control-E
        if key == 'ctrl e':
            self._app.make_pop_up(ActionsMenu(self))
            return None

        # send it to base class
        key = self.__super.keypress(size, key)
        return key

    def _counter(self) -> int:
        stat = self._db.archives_count(self._server, self._dataset, **self._db_options)
        return stat[0]

    def _pager(self, offset: int, pageSize: int) -> list[urwid.Widget]:
        items = self._db.archives(
            self._server, self._dataset, maxRows=pageSize, offset=offset, **self._db_options
        )
        return [self._widgetFactory(item) for item in items]

    def _widgetFactory(self, archive: CocaDb.Archive) -> urwid.Widget:
        _log.debug("CocaArchiveList: widget factory: %r", archive.name)
        is_open = " Yes" if archive.open_if_0 == 0 else " No "
        booked = archive.booked_at
        key = (archive.server_ipc, archive.dataset, archive.archive)
        checked = key in self._checked
        _log.debug("CocaArchiveList: checked: %r", checked)
        checkbox = SlimCheckBox('', checked)
        item = UIColumns([checkbox,
                          urwid.Text(str(archive.archive), align='right'),
                          archive.dataset_name, booked.date().isoformat(),
                          urwid.Text(f"{archive.archive_size_bytes / 1024. / 1024.:.2f}", align='right'),
                          is_open, archive.name],
                         self._col_width, 1, 0)

        urwid.connect_signal(checkbox, 'change', self._updateChecked, user_args=[archive])
        urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[archive])

        item = urwid.AttrMap(item, "list-item", "list-selected")
        item.user_data = key
        return item

    def _updateChecked(self, archive: CocaDb.Archive, checkbox: urwid.Widget, state: bool) -> None:
        key = (archive.server_ipc, archive.dataset, archive.archive)
        if state:
            self._checked.add(key)
        else:
            self._checked.discard(key)

    def _itemActivated(self, archive: CocaDb.Archive, item: urwid.Widget) -> None:
        self._emit('selected', archive)

    def _confirm_action(self, widget: urwid.Widget, action: str) -> None:
        _log.debug("CocaArchiveList: _confirm_action called: %s", action)

        if action == 'cancel':
            return

        selection = self._checked
        if not selection:
            item = self._listwalker.get_focus()[0]
            assert item is not None
            selection = set([item.user_data])

        if action == 'close':
            message = 'Do you want to mark {} as closed?'
        elif action == 'store':
            message = 'Do you want to mark {} as stored?'
        elif action == 'delete':
            message = 'Do you want to mark {} as removed?'
        else:
            return

        if len(selection) < 2:
            arc_msg = 'archive'
        else:
            arc_msg = '{} archives'.format(len(selection))
        message = message.format(arc_msg)

        callback = functools.partial(self._do_action, action, selection)
        self._app.make_pop_up(ConfirmAction(message, callback))

    def _do_action(self, action: str, selection: set[tuple[int, int, int]],
                   widget: urwid.Widget, confirmation: str) -> None:
        _log.debug("CocaArchiveList: _do_action called: %s %s %s", confirmation, action, selection)

        if confirmation != 'ok':
            return

        try:
            if action == 'close':
                n_updates = self._db.archives_set_closed(selection)
            elif action == 'store':
                n_updates = self._db.archives_set_stored(selection)
            elif action == 'delete':
                n_updates = self._db.archives_set_removed(selection)
            else:
                return

            message = '{} out of {} archives were updated'.format(n_updates, len(selection))
            self._app.make_pop_up(UpdateOK(message))

            # refresh current view
            self._total_count = self._counter()
            self._listwalker.reset(self._total_count)

        except Exception as exc:

            message = 'Exception caught while updating:\n' + str(exc)
            self._app.make_pop_up(UpdateFail(message))
