"""Module containing CocaServerList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from . import CocaDb

if TYPE_CHECKING:
    from . import CocaAdmin

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class CocaServerList(UIListBoxWithHeader, AppPanel):
    """Widget class containing list of CoCa servers.

    Parameters
    ----------
    app : `CocaAdmin`
    db : `CocaDb`
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CocaAdmin.CocaAdmin, db: CocaDb.CocaDb):
        self._app = app

        # read data from database
        servers = db.servers()
        self._numServers = len(servers)

        # add special entry at the top
        total_archives = sum(s.n_archives for s in servers)
        total_size = sum(s.totsize_bytes for s in servers)
        servers = [CocaDb.Server(server_ipc="", n_archives=total_archives, totsize_bytes=total_size)] + \
            sorted(servers)

        col_width1 = max(len(s.server_ipc or "<EVERYTHING>") for s in servers)
        col_width = [col_width1, 10, 15]
        header = UIColumns(['Server',
                            urwid.Text('N Archives', align='right'),
                            urwid.Text('Size, MB', align='right')],
                           col_width, 2)

        items = []
        for server in servers:
            name = server.server_ipc or "<EVERYTHING>"
            item = UIColumns([UISelectableText(name),
                              urwid.Text(str(int(server.n_archives)), align='right'),
                              urwid.Text(f"{server.totsize_bytes / 1024. / 1024.:.2f}", align='right')],
                             col_width, 2, 0)
            urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[server.server_ipc])
            items.append(item)

        UIListBoxWithHeader.__init__(self, items, header=header)

    def title(self) -> str:
        return 'List of CoCa servers'

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def status(self) -> str:
        return "Found {} servers".format(self._numServers)

    def _itemActivated(self, server_ipc: str, item: urwid.Widget) -> None:
        _log.debug("emitting signal 'selected': %r", server_ipc)
        self._emit('selected', server_ipc)
