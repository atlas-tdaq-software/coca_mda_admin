"""Module containing ServerList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import datetime
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBox, UIColumns

if TYPE_CHECKING:
    from . import CocaAdmin, CocaDb

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)


def _fmt(val: object) -> str:
    if isinstance(val, datetime.datetime):
        return val.isoformat(' ')
    elif isinstance(val, bool):
        return "Yes" if val else "No"
    else:
        return str(val)

#------------------------
# Exported definitions --
#------------------------


class CocaArchiveInfo(UIListBox, AppPanel):
    """
    Widget class info about single coca archive.
    """

    _selectable = True

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CocaAdmin.CocaAdmin, db: CocaDb.CocaDb, archive: CocaDb.Archive):
        """
        @param archive: CocaDb.Archive instance
        """

        files = db.files_in_archive(archive.dataset, archive.archive)

        items = [("Archive name:", archive.name),
                 ("Dataset name:", archive.dataset_name),
                 ("Archive number:", archive.archive),
                 ("Created:", archive.booked_at),
                 ("To be closed:", archive.close_at),
                 ("To be deleted:", archive.delete_at),
                 ("Server IPC:", archive.server_ipc_name),
                 ("Server host:", archive.server_host),
                 ("Server dir:", archive.dir_server),
                 ("Size, Bytes:", f"{archive.archive_size_bytes:n}"),
                 ("Closed:", archive.open_if_0 != 0),
                 ("Closed time:", archive.closed_at),
                 ("On CDR:", archive.on_cdr),
                 ("CDR time:", archive.on_cdr_at),
                 ("CDR path:", archive.cdr_path),
                 ("Stored:", archive.stored),
                 ("Stored time:", archive.stored_at),
                 ("Stored path:", archive.stor_path),
                 ("Removed:", archive.removed),
                 ("Removed time:", archive.removed_at)]
        for fname in files:
            items += [("Contains:", fname)]

        col_width = [15, -1]
        items = [UIColumns([urwid.Text(item[0], align='right'), urwid.Text(_fmt(item[1]))], col_width, 1)
                 for item in items]

        UIListBox.__init__(self, urwid.SimpleListWalker(items))

    def title(self) -> str:
        return "Archive information"
