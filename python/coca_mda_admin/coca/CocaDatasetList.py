"""Module containing CocaDatasetList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from . import CocaDb

if TYPE_CHECKING:
    from . import CocaAdmin

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class CocaDatasetList(UIListBoxWithHeader, AppPanel):
    """Widget class containing list of CoCa datasets.

    Parameters
    ----------
    app : `CocaAdmin`
    db: `CocaDb`
    server: `str`
        Name of coca server (server_ipc) or None.
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CocaAdmin.CocaAdmin, db: CocaDb.CocaDb, server: str):
        self._server = server

        # read data from database
        self._server = server
        datasets = db.datasets(server)
        self._numDatasets = len(datasets)

        # add special entry at the top
        total_archives = sum(ds.n_archives for ds in datasets)
        total_size = sum(ds.totsize_bytes for ds in datasets)
        datasets = [CocaDb.Dataset(name="", n_archives=total_archives, totsize_bytes=total_size)] + \
            sorted(datasets)

        col_width1 = max(len(ds.name or "<EVERYTHING>") for ds in datasets)
        col_width = [col_width1, 10, 15]
        header = UIColumns(['Dataset',
                            urwid.Text('N Archives', align='right'),
                            urwid.Text('Size, MB', align='right')],
                           col_width, 2)

        items = []
        for ds in datasets:
            name = ds.name or "<EVERYTHING>"
            item = UIColumns([UISelectableText(name),
                              urwid.Text(str(int(ds.n_archives)), align='right'),
                              urwid.Text(f"{ds.totsize_bytes / 1024. / 1024.:.2f}", align='right')],
                             col_width, 2, 0)
            urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[ds])
            items.append(item)

        UIListBoxWithHeader.__init__(self, items, header=header)

    def title(self) -> str:
        if self._server is None:
            return "Datasets for all servers"
        else:
            return "Datasets for server {!r}".format(self._server)

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def status(self) -> str:
        return "Found {} datasets".format(self._numDatasets)

    def _itemActivated(self, ds: CocaDb.Dataset, item: urwid.Widget) -> None:
        name = ds.name
        _log.debug("emitting signal 'selected': %r %r", self._server, name)
        self._emit('selected', self._server, name)
