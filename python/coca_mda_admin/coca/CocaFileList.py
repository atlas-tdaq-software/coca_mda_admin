"""Module containing CocaFileList class and related methods.
"""

from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import functools
import logging
from collections.abc import Callable
from typing import Any, TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import (UIListBoxWithHeader, UIColumns, UIPopUpMenu, UIPopUpMessageBox, SlimCheckBox)
from ..walkers import PageWalker

if TYPE_CHECKING:
    from . import CocaAdmin, CocaDb

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)


class ActionsMenu(UIPopUpMenu):

    _actions = [('Cancel', 'cancel'),
                ('Add to cache', 'add_to_cache'),
                ('Remove from client', 'delete_from_client'),
                ('Remove from cache', 'delete_from_cache'),
                ]

    def __init__(self, parent: urwid.Widget):
        self.__super.__init__("Actions", self._actions, parent._confirm_action, escape_value='cancel')


class ConfirmAction(UIPopUpMessageBox):

    def __init__(self, message: str, callback: Callable):
        buttons = [('Yes', 'ok'), ('No', 'cancel')]
        self.__super.__init__('Action confirmation', message, buttons, callback, escape_value='cancel')


class UpdateOK(UIPopUpMessageBox):

    def __init__(self, message: str):
        buttons = [('OK', 'ok')]
        self.__super.__init__('Success', message, buttons, escape_value='ok')


class UpdateFail(UIPopUpMessageBox):

    def __init__(self, message: str):
        buttons = [('Dismiss', 'ok')]
        self.__super.__init__('Failed', message, buttons, escape_value='ok', attr='messagebox-error')


#------------------------
# Exported definitions --
#------------------------

#---------------------
#  Class definition --
#---------------------
class CocaFileList(urwid.WidgetWrap, AppPanel):
    """Widget class containing list of CoCa files.

    Parameters
    ----------
    app : `CocaAdmin`
    db : `CocaDb`
    server : `str`
        name of coca server or None
    dataset : `str`
        name of coca dataset or None
    options: `dict`
        set of options passed to database API
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CocaAdmin.CocaAdmin, db: CocaDb.CocaDb,
                 server: str, dataset: str, options: dict[str, Any] = {}):
        self._app = app
        self._db = db
        self._server = server
        self._dataset = dataset
        self._col_width = [1, 20, 10, 5, -1]
        self._checked: set[tuple[int, str]] = set()
        self._db_options = options.copy()
        self._total_count = self._counter()
        self._listwalker = PageWalker(self._pager, self._total_count)

        header = UIColumns(['X', 'Dataset', 'Registered', 'Cache', 'Path'],
                           self._col_width, 1)

        lb = UIListBoxWithHeader(self._listwalker, header=header)
        self.__super.__init__(lb)

    def title(self) -> str:
        if self._server is None:
            return "Files for all servers"
        else:
            return "Files for server {!r}".format(self._server)

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Details"), ('Space', "Select"), ('^A', "Sel. All"), ('^E', "Action")]

    def status(self) -> str:
        status = "{} out of {} files".format(self._listwalker.focus + 1, self._total_count)
        if self._checked:
            status += " ({} checked)".format(len(self._checked))
        return status

    def keypress(self, size: tuple[int, int], key: str) -> str | None:

        # handle Control-E
        if key == 'ctrl e':
            self._app.make_pop_up(ActionsMenu(self))
            return None
        # handle Control-A
        if key == 'ctrl a':
            self._select_all()
            return None

        # send it to base class
        key = self.__super.keypress(size, key)
        return key

    def _counter(self) -> int:
        stat = self._db.files_count(self._server, self._dataset, **self._db_options)
        return stat[0]

    def _pager(self, offset: int, pageSize: int) -> list[urwid.Widget]:
        items = self._db.files(self._server, self._dataset,
                               maxRows=pageSize, offset=offset, **self._db_options)
        return [self._widgetFactory(item) for item in items]

    def _widgetFactory(self, filetuple: CocaDb.File) -> urwid.Widget:
        _log.debug("CocaFileList: widget factory: %r", filetuple.rel_path)
        registered = filetuple.registered_at
        in_cache = " Yes " if filetuple.in_cache else " No  "
        key = (filetuple.dataset, filetuple.rel_path)
        checked = key in self._checked
        _log.debug("CocaFileList: checked: %r", checked)
        checkbox = SlimCheckBox('', checked)
        item = UIColumns([checkbox,
                          filetuple.dataset_name,
                          registered.date().isoformat(),
                          in_cache,
                          filetuple.rel_path],
                         self._col_width, 1, 0)

        urwid.connect_signal(checkbox, 'change', self._updateChecked, user_args=[filetuple])
        urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[filetuple])

        item = urwid.AttrMap(item, "list-item", "list-selected")
        item.user_data = key
        return item

    def _updateChecked(self, filetuple: CocaDb.File, checkbox: urwid.Widget, state: bool) -> None:
        key = (filetuple.dataset, filetuple.rel_path)
        if state:
            self._checked.add(key)
        else:
            self._checked.discard(key)

    def _itemActivated(self, filetuple: CocaDb.File, item: urwid.Widget) -> None:
        self._emit('selected', filetuple)

    def _confirm_action(self, widget: urwid.Widget, action: str) -> None:
        _log.debug("CocaFileList: _confirm_action called: %s", action)

        if action == 'cancel':
            return

        selection = self._checked
        if not selection:
            item = self._listwalker.get_focus()[0]
            assert item is not None
            selection = set([item.user_data])

        if action == 'add_to_cache':
            message = 'Do you want to mark {} as being in cache?'
        elif action == 'delete_from_client':
            message = 'Do you want to mark {} as removed from client?'
        elif action == 'delete_from_cache':
            message = 'Do you want to mark {} as removed from cache?'
        else:
            return

        if len(selection) < 2:
            arc_msg = 'file'
        else:
            arc_msg = '{} files'.format(len(selection))
        message = message.format(arc_msg)

        callback = functools.partial(self._do_action, action, selection)
        self._app.make_pop_up(ConfirmAction(message, callback))

    def _do_action(self, action: str, selection: set[tuple[int, str]],
                   widget: urwid.Widget, confirmation: str) -> None:
        _log.debug("ArchiveList: _do_action called: %s %s %s", confirmation, action, selection)

        if confirmation != 'ok':
            return

        try:
            if action == 'add_to_cache':
                n_updates = self._db.files_add_to_cache(selection)
                self._checked = set()
            elif action == 'delete_from_client':
                n_updates = self._db.files_remove_from_client(selection)
                self._checked = set()
            elif action == 'delete_from_cache':
                n_updates = self._db.files_remove_from_cache(selection)
                self._checked = set()
            else:
                return

            message = '{} out of {} files were updated'.format(n_updates, len(selection))
            self._app.make_pop_up(UpdateOK(message))

            # refresh current view
            self._total_count = self._counter()
            self._listwalker.reset(self._total_count)

        except Exception as exc:

            message = 'Exception caught while updating:\n' + str(exc)
            self._app.make_pop_up(UpdateFail(message))

    def _select_all(self) -> None:
        _log.debug("ArchiveList: _select_all")

        if self._checked:
            self._checked = set()
        else:
            items = self._db.files(self._server, self._dataset, **self._db_options)
            for filetuple in items:
                key = (filetuple.dataset, filetuple.rel_path)
                self._checked.add(key)

        # refresh current view
        self._total_count = self._counter()
        self._listwalker.reset(self._total_count)
