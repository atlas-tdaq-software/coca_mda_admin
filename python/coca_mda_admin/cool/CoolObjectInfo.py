"""Module containing CocaFileInfo class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from collections.abc import Iterable
from typing import TYPE_CHECKING, cast

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBox, UIColumns
from . import CoolDb
from coldpie import coral, cool

if TYPE_CHECKING:
    from . import CoolTui

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_ST = CoolDb.StorageType

_log = logging.getLogger(__name__)


def _storageTypeName(value: int) -> str:
    """Guess storage type name from the value.
    """
    for k, v in vars(_ST).items():
        if not k.startswith("_") and v == value:
            return k
    return str(value)

#------------------------
# Exported definitions --
#------------------------


class CoolObjectInfo(urwid.WidgetWrap, AppPanel):
    """Widget class info about single cool object (and its interval).

    Parameters
    ----------
    app : `CoolTui`
    db: `CoolDb`
    folder : `CoolDb.Folder`
    tag : `CoolDb.TagInfo`
    channel : `CoolDb.Channel`
    iovs : `list` of `CoolDb.IOV`
    idx : `int`
        Current selected position in `iovs` list
    """

    _selectable = True

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CoolTui.CoolTui, db: CoolDb.CoolDb, folder: CoolDb.Folder,
                 tag: CoolDb.TagInfo | None, channel: CoolDb.Channel,
                 iovs: Iterable[CoolDb.IOV], idx: int):
        self._app = app
        self._db = db
        self._folder = folder
        self._tag = tag
        self._channel = channel
        self._iovs = list(iovs)
        self._idx = idx
        self._initial = idx

        self._topItems = dict(iov_since=urwid.Text(""),
                              iov_until=urwid.Text(""),
                              inserted=urwid.Text(""),
                              channel_id=urwid.Text(""))
        col_width = [15, -1]
        self._items = urwid.SimpleListWalker([])
        self._items += [UIColumns([urwid.Text("IOV Since:", align='right'), self._topItems["iov_since"]],
                                  col_width, 1),
                        UIColumns([urwid.Text("IOV Until:", align='right'), self._topItems["iov_until"]],
                                  col_width, 1),
                        UIColumns([urwid.Text("Inserted:", align='right'), self._topItems["inserted"]],
                                  col_width, 1),
                        UIColumns([urwid.Text("Channel ID:", align='right'), self._topItems["channel_id"]],
                                  col_width, 1)]
        self._items.append(urwid.Divider(' '))
        listbox = UIListBox(self._items)
        self.__super.__init__(listbox)

        self._showInfo()

    def title(self) -> str:
        return "COOL Object Information"

    def hints(self) -> list[tuple[str, str]]:
        return [('->', "Next"), ('<-', "Prev"), ('^', "Initial")]

    def status(self) -> str:
        if self._idx == 0:
            return "First interval"
        elif self._idx == len(self._iovs) - 1:
            return "Last interval"
        else:
            return "Interval {} out of {}".format(self._idx + 1, len(self._iovs))

    def keypress(self, size: tuple[int, int], key: str) -> str | None:

        # handle next/prev
        if key == 'right':
            if self._idx < len(self._iovs) - 1:
                self._idx += 1
                self._showInfo()
            return None
        elif key == 'left':
            if self._idx > 0:
                self._idx -= 1
                self._showInfo()
            return None
        elif key == '^':
            if self._idx > 0:
                self._idx = self._initial
                self._showInfo()
            return None

        # send it to base class
        key = self.__super.keypress(size, key)
        return key

    def _showInfo(self) -> None:

        key = self._iovs[self._idx].since
        obj = self._db.getObject(self._folder, key, self._channel.id,
                                 "" if self._tag is None else self._tag.name)
        if obj is None:
            return

        self._topItems["iov_since"].set_text(self._fmtIOVkey(obj.since))
        self._topItems["iov_until"].set_text(self._fmtIOVkey(obj.until))
        self._topItems["inserted"].set_text(str(obj.inserted))
        self._topItems["channel_id"].set_text(str(obj.channelId))

        fields_col_width = [12, 10, -1]
        items = []
        payload = obj.payload
        _log.debug("payload: %r", payload)
        for field in payload:
            _log.debug("field: %r", field)
            typeName = _storageTypeName(field.storageType())
            fields_col_width[0] = max(fields_col_width[0], min(32, len(field.name())))
            fields_col_width[1] = max(fields_col_width[1], len(typeName))
            value = self._fmtField(self._folder, field)
            items += [(field.name(), typeName, value)]

        header = UIColumns([urwid.Text('Attribute'),
                            urwid.Text('Type'),
                            urwid.Text('Value')],
                           fields_col_width, 2)
        self._items[5:] = [urwid.AttrMap(header, 'list-colhead')] + \
                          [UIColumns([urwid.Text(item[0]),
                                      urwid.Text(item[1]),
                                      urwid.Text(item[2])],
                                     fields_col_width, 2)
                           for item in items]

    def _fmtIOVkey(self, key: int) -> str:
        if key == CoolDb.ValidityKeyMin:
            return "Min"
        elif key == CoolDb.ValidityKeyMax:
            return "Max"
        elif self._folder.iov_type == CoolDb.IOV_RUN_LUMI:
            run, lb = key // 2**32, key % 2**32
            if lb == 2**32 - 1:
                return f"Run={run} LB=Max (Key={key})"
            return f"Run={run} LB={lb} (Key={key})"
        elif self._folder.iov_type == CoolDb.IOV_RUN_EVENT:
            run, evt = key // 2**32, key % 2**32
            if evt == 2**32 - 1:
                return f"Run={run} Event=Max (Key={key})"
            return f"Run={run} Event={evt} (Key={key})"
        elif self._folder.iov_type == CoolDb.IOV_TIME:
            # nanoseconds, convert to COOL time
            dt = cool.Time(key)
            return "{} (Key={})".format(dt, key)
        else:
            return "{}".format(key)

    def _fmtField(self, folder: CoolDb.Folder, field: cool.Field) -> str:
        if field.isNull():
            value = "<NULL>"
        elif field.storageType() in (_ST.Blob64k, _ST.Blob16M):
            blob = cast(coral.Blob, field.data())
            size = blob.size()
            blob_bytes: bytes
            if size > 256:
                blob_bytes = blob.read(253)
            else:
                blob_bytes = blob.readline()
            if blob_bytes is None:
                value = "<EMPTY>"
            else:
                values = ["{:02x}".format(b) for b in blob_bytes]
                value = " ".join(values)
            if size > 256:
                value += " ..."
        elif field.storageType() in (_ST.String255, _ST.String4k, _ST.String64k, _ST.String16M):
            value = cast(str, field.data())
            if len(value) > 512:
                value = value[:509] + "..."
        elif field.name() == "RunLB" and field.storageType() == CoolDb.StorageType.UInt63:
            runlb = cast(int, field.data())
            run, lbn = runlb // 2**32, runlb % 2**32
            value = "{} (Run={} LB={})".format(runlb, run, lbn)
        else:
            value = str(field.data())
        return value
