"""Module containing CoolChannelList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
import time
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText, UIPopUpProgress

if TYPE_CHECKING:
    from . import CoolDb, CoolTui

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class CoolChannelList(urwid.WidgetWrap, AppPanel):
    """Widget class containing list COOL channels for a folder.

    Parameters
    ----------
    app: `CoolTui`
        application class
    db: `CoolDb`
        instance of CoolDb class
    folder : `CoolDb.Folder`
    tag : `CoolDb.TagInfo`
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CoolTui.CoolTui, db: CoolDb.CoolDb, folder: CoolDb.Folder,
                 tag: CoolDb.TagInfo | None, main_loop: urwid.MainLoop | None = None):
        self._app = app
        self._db = db
        self._folder = folder
        self._tag = tag
        self._main_loop = main_loop
        self._col_width = [12, -1, -1]

        header = UIColumns([urwid.Text('Channel#', align="right"),
                            urwid.Text('Channel Name'),
                            urwid.Text('Description')],
                           self._col_width, 2)

        self.items = urwid.SimpleFocusListWalker([])
        lb = UIListBoxWithHeader(self.items, header=header)
        super().__init__(lb)

        progress_bar = None
        num_channels = self._db.folderChannelCount(self._folder)
        ch_iter = self._db.folderChannels(self._folder)
        t0 = time.time()
        for channel in ch_iter:
            item = UIColumns([UISelectableText(str(channel.id), align="right"),
                              urwid.Text(channel.name),
                              urwid.Text(channel.description)],
                             self._col_width, 2, 0)
            urwid.connect_signal(item, 'activated', self._channelActivated, user_args=[channel])
            item = urwid.AttrMap(item, "list-item", "list-selected")
            self.items += [item]

            if time.time() - t0 > 0.2:
                if progress_bar is None:
                    progress_bar = UIPopUpProgress("Reading channels", done=num_channels)
                    app.make_pop_up(progress_bar)
                progress_bar.set_completion(len(self.items))
                if main_loop:
                    main_loop.draw_screen()
                t0 = time.time()

        if progress_bar is not None:
            progress_bar.close()

    def title(self) -> str:
        return "Channels in " + self._db.name + ":" + self._folder.name

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def _channelActivated(self, channel: CoolDb.Channel, item: urwid.Widget) -> None:
        _log.debug("emitting signal 'selected': %r", channel)
        self._emit('selected', self._db, self._folder, self._tag, channel)
