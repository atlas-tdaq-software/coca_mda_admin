"""Module containing CoolTagList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from . import CoolDb

if TYPE_CHECKING:
    from . import CoolTui

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

_EMPTY_TAG_NAME = "<EMPTY>"

#------------------------
# Exported definitions --
#------------------------


class CoolTagList(urwid.WidgetWrap, AppPanel):
    """Widget class containing list COOL tags for a folder.

    Parameters
    ----------
    app: `CoolTui`
        application class
    db: `CoolDb`
        instance of CoolDb class
    folder : `CoolDb.Folder`
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CoolTui.CoolTui, db: CoolDb.CoolDb, folder: CoolDb.Folder):
        self._app = app
        self._db = db
        self._folder = folder
        self._col_width = [-1, 2, 33, -1]

        # Add special empty tag which may exist in some folders but is not returned
        # by COOL so there is no corresponding entry in `folder.tags`
        emptyTag = CoolDb.TagInfo(name=_EMPTY_TAG_NAME, description="Default empty tag", dt="", lock="?")
        items = []
        for tag in folder.tags + [emptyTag]:
            item = UIColumns([UISelectableText(tag.name),
                              urwid.Text(tag.lock),
                              urwid.Text(str(tag.dt)),
                              urwid.Text(tag.description),
                              ],
                             self._col_width, 2, 0)
            urwid.connect_signal(item, 'activated', self._tagActivated, user_args=[tag])
            item = urwid.AttrMap(item, "list-item", "list-selected")
            items += [item]

        header = UIColumns([urwid.Text('Tag Name'),
                            urwid.Text('LK'),
                            urwid.Text('Tag Insert Time'),
                            urwid.Text('Description'),
                            ],
                           self._col_width, 2)

        lb = UIListBoxWithHeader(items, header=header)
        self.__super.__init__(lb)

    def title(self) -> str:
        return "Tags in " + self._db.name + ":" + self._folder.name

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def _tagActivated(self, tag: CoolDb.TagInfo, item: urwid.Widget) -> None:
        _log.debug("emitting signal 'selected': %r", tag)
        nullTag: CoolDb.TagInfo | None = None
        if tag.name != _EMPTY_TAG_NAME:
            nullTag = tag
        self._emit('selected', self._db, self._folder, nullTag)
