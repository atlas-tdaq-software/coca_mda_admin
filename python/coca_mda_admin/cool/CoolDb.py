"""Module containing CoolDb class and related methods.
"""

from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
import re
from collections import namedtuple
from collections.abc import Iterator
from contextlib import contextmanager

#-----------------------------
# Imports for other modules --
#-----------------------------
from coldpie import cool, coral

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)


@contextmanager
def read_transaction(session: coral.ISessionProxy) -> Iterator:
    readonly = True
    trans = session.transaction()
    trans.start(readonly)
    yield trans
    trans.commit()


@contextmanager
def update_transaction(session: coral.ISessionProxy) -> Iterator:
    readonly = False
    trans = session.transaction()
    trans.start(readonly)
    yield trans
    trans.commit()


def _fixConnStr(conn_str: str, dbName: str) -> str:
    """Convert CORAL-style connection string into COOL format"""
    if conn_str.startswith("sqlite_file:"):
        scheme, _, loc = conn_str.partition(":")
        if loc.startswith("//"):
            # replace multiple leading slashes with one
            loc = "/" + loc.lstrip("/")
        conn_str = scheme + ":" + loc + "/" + dbName
    elif "://" in conn_str:
        server, schema = conn_str.rsplit("/", 1)
        conn_str = server + ";schema=" + schema + ";dbname=" + dbName
    else:
        conn_str += "/" + dbName
    return conn_str


def _iov_type(folder_description: str) -> str:
    """Guess folder IOV type from folder description.
    """
    for ktype in (IOV_RUN_LUMI, IOV_TIME, IOV_RUN_EVENT):
        kdesc = "<timeStamp> *" + ktype + " *</timeStamp>"
        if re.search(kdesc, folder_description):
            return ktype
    return IOV_UNKNOWN

#------------------------
# Exported definitions --
#------------------------


ValidityKeyMin = cool.ValidityKeyMin
ValidityKeyMax = cool.ValidityKeyMax
StorageType = cool.StorageType

# folder IOV key type
IOV_RUN_LUMI = "run-lumi"
IOV_RUN_EVENT = "run-event"  # used in some old folders
IOV_TIME = "time"
IOV_UNKNOWN = "unknown"

FolderSet = namedtuple("FolderSet", "name description")
# multi_version: bool
Folder = namedtuple("Folder", "name description payload_spec multi_version tags attributes iov_type")
TagInfo = namedtuple("TagInfo", "name description dt lock")
FieldSpec = namedtuple("FieldSpec", "name storage_type")
Channel = namedtuple("Channel", "id name description")
IOV = namedtuple("IOV", "since until inserted")
Object = namedtuple("Object", "since until payload inserted channelId")


def getDatabaseNames(conn_str: str) -> list[str]:
    """Return the list of COOL database name for a given schema.

    @param conn_str:  CORAL connection string (not COOL)
    """
    svc = coral.ConnectionService()
    sess = svc.connect(conn_str, accessMode=0)
    with read_transaction(sess):
        tables = sess.nominalSchema().listTables()
    sfx = "_DB_ATTRIBUTES"
    return [t[:-len(sfx)] for t in tables if t.endswith(sfx)]


class CoolDb:
    """Interface for COOL database.

    Parameters
    ----------
    conn_str: `str`
        CORAL connection string (not COOL)
    dbName: `str`
        COOL database name
    readonly: `boolean`
        if True then use read-only mode for database
    """

    def __init__(self, conn_str: str, dbName: str, readonly: bool = True):

        logging.debug("opening database: %s", conn_str)
        db_svc = cool.DatabaseSvcFactory.databaseService()
        self.db = db_svc.openDatabase(_fixConnStr(conn_str, dbName), readonly)
        self.name = dbName

    def lsFolder(self, folderSet: FolderSet | None = None) -> tuple[list[FolderSet], list[Folder]]:
        """List folder contents.

        Parameters
        ----------
        folderSet : `FolderSet`, optional

        Returns
        -------
        foldersets : `list` of `FolderSet`
        folders : `list` of `Folder`
        """
        name = "/" if folderSet is None else folderSet.name
        fs = self.db.getFolderSet(name)

        foldersets = []
        folders = []
        for name in fs.listFolderSets():
            fs = self.db.getFolderSet(name)
            foldersets.append(FolderSet(name=name, description=fs.description()))
        for name in fs.listFolders():
            f = self.db.getFolder(name)
            mv = f.versioningMode() == cool.FolderVersioning.MULTI_VERSION
            tags = self._folderTags(f) if mv else None
            payload_spec = self._payloadSpec(f)
            attr = dict((field.name(), field.data()) for field in f.folderAttributes())
            iov_type = _iov_type(f.description())
            folders.append(Folder(name=name, description=f.description(), payload_spec=payload_spec,
                                  multi_version=mv, tags=tags, attributes=attr, iov_type=iov_type))

        return foldersets, folders

    def _folderTags(self, folder: cool.Folder) -> list[TagInfo]:
        tags = []
        for tag in folder.listTags():
            descr = folder.tagDescription(tag)
            lock = folder.tagLockStatus(tag)
            lock_str = {cool.HvsTagLock.UNLOCKED: 'N', cool.HvsTagLock.LOCKED: 'Y'}.get(lock, "?")
            time = folder.tagInsertionTime(tag)
            tags += [TagInfo(name=tag, description=descr, dt=time, lock=lock_str)]
        return tags

    def _payloadSpec(self, folder: cool.Folder) -> list[FieldSpec]:
        spec = []
        for fs in folder.payloadSpecification():
            spec += [FieldSpec(name=fs.name(), storage_type=fs.storageType())]
        return spec

    def folderChannels(self, folder: Folder) -> Iterator[Channel]:
        """Return list of channels for a folder.

        Parameters
        ----------
        folder : `Folder`

        Returns
        -------
        iterator of `Channel` objects
        """
        f = self.db.getFolder(folder.name)
        f.setPrefetchAll(False)
        for ch in f.listChannels():
            yield Channel(id=ch, name=f.channelName(ch), description=f.channelDescription(ch))

    def folderChannelCount(self, folder: Folder) -> int:
        """Return number of channels for a folder.

        Parameters
        ----------
        folder : `Folder`

        Returns
        -------
        Number of channels in a folder.
        """
        f = self.db.getFolder(folder.name)
        return len(f.listChannels())

    def folderIOVs(self, folder: Folder, channel: int, tag: str,
                   since: int = cool.ValidityKeyMin, until: int = cool.ValidityKeyMax) -> Iterator[IOV]:
        """Return list of IOVs for a folder.

        Parameters
        ----------
        folder : `Folder`
        channel : `int`
        tag : `str`, optional

        Returns
        -------
        iterator of `IOV`
        """
        _log.debug("folderIOVs: %r %r %r", folder, channel, tag)
        f = self.db.getFolder(folder.name)
        f.setPrefetchAll(False)
        channels = cool.ChannelSelection(channel)
        itr = f.browseObjects(since, until, channels, tag)
        for obj in itr:
            _log.debug("folderIOVs: next iteration: %r", obj)
            itime = obj.insertionTime()
            yield IOV(obj.since(), obj.until(), inserted=itime)

    def folderIOVCount(self, folder: Folder, channel: int, tag: str,
                       since: int = cool.ValidityKeyMin, until: int = cool.ValidityKeyMax) -> int:
        """Return count of IOVs for a folder.

        Parameters
        ----------
        folder : `Folder`
        channel : `int`
        tag : `str`, optional

        Returns
        -------
        NUmber of IOVs.
        """
        _log.debug("folderIOVCount: %r %r %r", folder, channel, tag)
        f = self.db.getFolder(folder.name)
        channels = cool.ChannelSelection(channel)
        return f.countObjects(since, until, channels, tag)

    def getObject(self, folder: Folder, key: int, channel: int, tag: str) -> Object | None:
        """Return object for a given key.

        Parameters
        ----------
        folder : `Folder`
        key : `int`
            Validity "time"
        channel : `int`
        tag : `str`, optional

        Returns
        -------
        object : `Object`
        """
        f = self.db.getFolder(folder.name)
        channels = cool.ChannelSelection(channel)
        itr = f.browseObjects(key, key, channels, tag)
        for obj in itr:
            _log.debug("obj: %r", obj)
            return Object(since=obj.since(),
                          until=obj.until(),
                          payload=obj.payload(),
                          inserted=obj.insertionTime(),
                          channelId=obj.channelId())
        return None


#
# This code is unused currently
#
def _splitInterval(folder: cool.Folder, channels: cool.ChannelSelection, tag: str,
                   keyMin: int, keyMax: int, minIntervalSize: int,
                   maxIOVsPerBin: int = 0) -> list[tuple[int, int, int]]:
    """Split given range of keys into a number of intervals so that each
    interval contains small number of IOVs.

    Parameters
    ----------
    folder : `cool.IFolder`
    channels : `cool.ChannelSelection`
    tag : `str`
    keyMin, keyMax : `int`
        Minumum and maximum starting keys for the interval.
    maxIOVsPerBin : `int`, optional
        Maximum number of IOVs in resulting intervals, if 0 then it is set
        to some predefined value.

    Returns
    -------
    intervals : `list` of `tuple`
        each tuple in a list contains three numbers (since, until, numIOVs).
    """
    currentCount = folder.countObjects(keyMin, keyMax, channels, tag)
    if maxIOVsPerBin == 0:
        # 50-256 depends on folder population
        maxIOVsPerBin = min(currentCount // 100, 512)
        maxIOVsPerBin = max(maxIOVsPerBin, 50)
        # print("maxIOVsPerBin:", maxIOVsPerBin)

    # print("keyMin, keyMax, currentCount:", keyMin, keyMax, currentCount)
    # print("keyMax - keyMin, minIntervalSize:", keyMax - keyMin, minIntervalSize)
    if currentCount <= maxIOVsPerBin or keyMax - keyMin <= minIntervalSize:
        # cannot split further
        # print("return:", (keyMin, keyMax, currentCount))
        return [(keyMin, keyMax, currentCount)]

    # split in the middle
    mid = (keyMin + keyMax) // 2
    intervals = _splitInterval(folder, channels, tag, keyMin, mid, minIntervalSize, maxIOVsPerBin) + \
        _splitInterval(folder, channels, tag, mid, keyMax, minIntervalSize, maxIOVsPerBin)
    return intervals
