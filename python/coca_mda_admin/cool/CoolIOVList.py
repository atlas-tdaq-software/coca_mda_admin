"""Module containing CoolIOVList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
import time
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText, UIPopUpProgress
from . import CoolDb
from coldpie import cool

if TYPE_CHECKING:
    from . import CoolTui

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class CoolIOVList(urwid.WidgetWrap, AppPanel):
    """Widget class containing list COOL channels for a folder.

    Parameters
    ----------
    app: `CoolTui`
        application class
    db: `CoolDb`
        instance of CoolDb class
    folder : `CoolDb.Folder`
    tag : `CoolDb.TagInfo`
    channel : `CoolDb.Channel`
    main_loop : `urwid.MainLoop`
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CoolTui.CoolTui, db: CoolDb.CoolDb, folder: CoolDb.Folder,
                 tag: CoolDb.TagInfo | None, channel: CoolDb.Channel,
                 since: int = CoolDb.ValidityKeyMin, until: int = CoolDb.ValidityKeyMax,
                 main_loop: urwid.MainLoop | None = None):
        self._app = app
        self._db = db
        self._folder = folder
        self._tag = tag
        self._channel = channel
        self._main_loop = main_loop

        tagName = tag.name if tag else ""

        # read all IOVs from database, but show progress bar
        progress_bar = UIPopUpProgress("Reading IOVs", done=100)
        app.make_pop_up(progress_bar)
        if main_loop:
            main_loop.draw_screen()
        self._num_iovs = self._db.folderIOVCount(self._folder, channel.id, tagName, since, until)
        progress_bar.set_done(self._num_iovs or 1)
        if main_loop:
            main_loop.draw_screen()

        iov_iter = self._db.folderIOVs(self._folder, channel.id, tagName, since, until)
        t0 = time.time()
        self._iovs = []
        for iov in iov_iter:
            self._iovs.append(iov)
            if time.time() - t0 > 0.2:
                progress_bar.set_completion(len(self._iovs))
                if main_loop:
                    main_loop.draw_screen()
                t0 = time.time()

        if progress_bar is not None:
            progress_bar.close()

        self._col_width = [7, 33, 33, 33]
        header = UIColumns([urwid.Text('#', align="right"),
                            urwid.Text(self._fmtColHdr('Since')),
                            urwid.Text(self._fmtColHdr('Until')),
                            urwid.Text('Insert Time')],
                           self._col_width, 2)

        items = []
        for i, iov in enumerate(self._iovs):
            item = UIColumns([UISelectableText(str(i), align="right"),
                              urwid.Text(self._fmtIOVkey(iov.since)),
                              urwid.Text(self._fmtIOVkey(iov.until)),
                              urwid.Text(str(iov.inserted))],
                             self._col_width, 2, 0)
            urwid.connect_signal(item, 'activated', self._iovActivated, user_args=[i, iov])
            items += [urwid.AttrMap(item, "list-item", "list-selected")]

        lb = UIListBoxWithHeader(items, header=header)
        self.__super.__init__(lb)

    def title(self) -> str:
        return "IOVs in " + self._db.name + ":" + self._folder.name

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def status(self) -> str:
        status = "{} intervals ".format(self._num_iovs)
        if self._tag:
            status += "[tag={}]".format(self._tag.name)
        status += "[channel={}]".format(self._channel.id)
        return status

    def keypress(self, size: tuple[int, int], key: str) -> str | None:

        # send it to base class
        key = self.__super.keypress(size, key)
        return key

    def _iovActivated(self, idx: int, iov: CoolDb.IOV, item: urwid.Widget) -> None:
        _log.debug("emitting signal 'selected': %r %r", idx, iov)
        self._emit('selected', self._db, self._folder, self._tag, self._channel, self._iovs, idx)

    def _fmtIOVkey(self, key: int) -> str:
        if key == CoolDb.ValidityKeyMin:
            return "Min"
        elif key == CoolDb.ValidityKeyMax:
            return "Max"
        elif self._folder.iov_type == CoolDb.IOV_RUN_LUMI:
            run, lb = key // 2**32, key % 2**32
            if lb == 2**32 - 1:
                return f"{run}-Max"
            return f"{run}-{lb}"
        elif self._folder.iov_type == CoolDb.IOV_RUN_EVENT:
            run, evt = key // 2**32, key % 2**32
            if evt == 2**32 - 1:
                return f"{run}-Max"
            return f"{run}-{evt}"
        elif self._folder.iov_type == CoolDb.IOV_TIME:
            # nanoseconds, convert to local time
            return str(cool.Time(key))
        else:
            return str(key)

    def _fmtColHdr(self, hdr: str) -> str:
        if self._folder.iov_type == CoolDb.IOV_RUN_LUMI:
            return hdr + "(Run-LB)"
        elif self._folder.iov_type == CoolDb.IOV_RUN_EVENT:
            return hdr + "(Run-Event)"
        elif self._folder.iov_type == CoolDb.IOV_TIME:
            return hdr + "(Time)"
        else:
            return hdr
