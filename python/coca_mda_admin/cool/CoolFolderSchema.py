"""Module containing CoolFolderSchema class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from . import CoolDb

if TYPE_CHECKING:
    from . import CoolTui

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)


def _storageTypeName(value: int) -> str:
    """Guess storage type name from the value.
    """
    for k, v in vars(CoolDb.StorageType).items():
        if not k.startswith("_") and v == value:
            return k
    return str(value)

#------------------------
# Exported definitions --
#------------------------


class CoolFolderSchema(urwid.WidgetWrap, AppPanel):
    """Widget class containing list COOL tags for a folder.

    Parameters
    ----------
    app: `CoolTui`
        application class
    db: `CoolDb`
        instance of CoolDb class
    folder : `CoolDb.Folder`
    """

    _selectable = True

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CoolTui.CoolTui, db: CoolDb.CoolDb, folder: CoolDb.Folder):
        self._app = app
        self._db = db
        self._folder = folder
        self._col_width = [2, -1, -2]

        items = []
        for i, fs in enumerate(folder.payload_spec):
            item = UIColumns([UISelectableText(str(i + 1), align="right"),
                              urwid.Text(fs.name),
                              urwid.Text(_storageTypeName(fs.storage_type))],
                             self._col_width, 2, 0)
            items += [item]

        header = UIColumns([urwid.Text('#', align="right"),
                            urwid.Text('Field'),
                            urwid.Text('Storage Type'),
                            ],
                           self._col_width, 2)

        lb = UIListBoxWithHeader(items, header=header)
        self.__super.__init__(lb)

    def title(self) -> str:
        return "Folder schema in " + self._db.name + ":" + self._folder.name
