"""Module for CoolTui class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import Any

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppBase import AppBase
from ..ui import UIPopUpProgress, UIPopUpKeysInput, UIPopUpKeysInputHelp
from . import CoolDb
from .CoolChannelList import CoolChannelList
from .CoolDbList import CoolDbList
from .CoolFolderList import CoolFolderList
from .CoolFolderSchema import CoolFolderSchema
from .CoolIOVList import CoolIOVList
from .CoolObjectInfo import CoolObjectInfo
from .CoolTagList import CoolTagList

#----------------------------------
# Local non-exported definitions --
#----------------------------------
_palette = [
    ('folder-item', 'black', 'light gray'),
    ('folder-selected', 'white', 'dark gray', 'standout'),
    ('folderset-item', 'dark blue', 'light gray'),
    ('folderset-selected', 'light cyan', 'dark gray', 'standout'),
]

_log = logging.getLogger(__name__)

IOV_COUNT_CHECK = 10000

#------------------------
# Exported definitions --
#------------------------


class CoolTui(AppBase):
    """Class representing top-level widget for the whole application.

    Parameters
    ----------
    conn_str: `str`
        CORAL connection string for database
    readonly: `bool`
        if True then use read-only mode for database
    """

    # extend default palette
    palette = AppBase.palette + _palette

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, conn_str: str, readonly: bool):
        AppBase.__init__(self)
        self._conn_str = conn_str

    def title(self) -> str:
        """Return string displayed as application title at the top
        of the window.

        If None or empty string is returned then nothing is displayed.
        """
        return ' = COOL browser = '

    def start(self, loop: urwid.MainLoop, user_data: Any) -> None:
        """Make initial panel
        """
        dbList = self.makePanel(CoolDbList, self._conn_str)
        urwid.connect_signal(dbList, 'selected', self.databaseSelected)

    def databaseSelected(self, widget: urwid.Widget, dbName: str) -> None:
        _log.debug("selected database: %r", dbName)
        db = CoolDb.CoolDb(self._conn_str, dbName)
        widget = self.makePanel(CoolFolderList, db)
        urwid.connect_signal(widget, 'folderSelected', self.folderSelected)
        urwid.connect_signal(widget, 'folderSetSelected', self.folderSetSelected)

    def folderSelected(self, widget: urwid.Widget, db: CoolDb.CoolDb, folder: CoolDb.Folder) -> None:
        _log.debug("selected folder: %r", folder)

        if folder.tags is not None:
            # for MV folders show their tags
            widget = self.makePanel(CoolTagList, db, folder)
            urwid.connect_signal(widget, 'selected', self.tagSelected)
        else:
            # just show intervals for tag=None
            self.tagSelected(widget, db, folder, tag=None)

    def folderSetSelected(self, widget: urwid.Widget, db: CoolDb.CoolDb, folderset: CoolDb.FolderSet) -> None:
        _log.debug("selected folder: %r", folderset)
        widget = self.makePanel(CoolFolderList, db, folderset)
        urwid.connect_signal(widget, 'folderSelected', self.folderSelected)
        urwid.connect_signal(widget, 'folderSetSelected', self.folderSetSelected)
        urwid.connect_signal(widget, 'showSchema', self.showFolderSchema)

    def showFolderSchema(self, widget: urwid.Widget, db: CoolDb.CoolDb, folder: CoolDb.Folder) -> None:
        self.makePanel(CoolFolderSchema, db, folder)

    def tagSelected(self, widget: urwid.Widget, db: CoolDb.CoolDb, folder: CoolDb.Folder,
                    tag: str | None) -> None:
        _log.debug("selected tag: %r", tag)
        widget = self.makePanel(CoolChannelList, db, folder, tag, self._main_loop)
        urwid.connect_signal(widget, 'selected', self.channelSelected)

    def channelSelected(self, widget: urwid.Widget, db: CoolDb.CoolDb, folder: CoolDb.Folder,
                        tag: CoolDb.TagInfo | None, channel: CoolDb.Channel) -> None:
        _log.debug("selected channel: %r", channel)

        # Scanning whole IOV range is very slow for some folders, we have to
        # restrict IOV range to shorter interval so we can get response reasonably
        # fast. This method tries asks user for the range of runs or times if
        # it decides that limiting the range is the right thing to do.
        tagName = tag.name if tag else ""

        # first count the total number of intervals, this should be relatively fast
        progress_bar = UIPopUpProgress("Counting IOVs", done=100)
        self.make_pop_up(progress_bar)
        if self._main_loop:
            self._main_loop.draw_screen()
        num_iovs = db.folderIOVCount(folder, channel.id, tagName)
        progress_bar.set_completion(100)
        if self._main_loop:
            self._main_loop.draw_screen()
        progress_bar.close()

        if num_iovs < IOV_COUNT_CHECK:
            # cover whole range
            widget = self.makePanel(CoolIOVList, db, folder, tag, channel, main_loop=self._main_loop)
            urwid.connect_signal(widget, 'selected', self.iovSelected)
            return

        msg = "Please select IOV keys range for next operation."
        keys_input = UIPopUpKeysInput("Interval select", msg, folder.iov_type)
        self.make_pop_up(keys_input)
        urwid.connect_signal(keys_input, 'selected', self.iovRangeSelected,
                             user_args=[db, folder, tag, channel])
        urwid.connect_signal(keys_input, 'help', self.iovSelectionHelp)

    def iovSelectionHelp(self, widget: urwid.Widget) -> None:
        help_popup = UIPopUpKeysInputHelp()
        self.make_pop_up(help_popup)

    def iovRangeSelected(self, db: CoolDb.CoolDb, folder: CoolDb.Folder, tag: CoolDb.TagInfo | None,
                         channel: CoolDb.Channel, widget: urwid.Widget, since: int, until: int) -> None:
        _log.debug("selected iov range: %r %r %r %r", db, folder, tag, channel)
        _log.debug("selected iov range: %r %r", since, until)
        widget = self.makePanel(CoolIOVList, db, folder, tag, channel,
                                since=since, until=until, main_loop=self._main_loop)
        urwid.connect_signal(widget, 'selected', self.iovSelected)

    def iovSelected(self, widget: urwid.Widget, db: CoolDb.CoolDb, folder: CoolDb.Folder,
                    tag: CoolDb.TagInfo | None, channel: CoolDb.Channel,
                    iovs: list[CoolDb.IOV], idx: int) -> None:
        _log.debug("selected iov index: %r", idx)
        self.makePanel(CoolObjectInfo, db, folder, tag, channel, iovs, idx)
