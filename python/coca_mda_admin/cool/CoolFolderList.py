"""Module containing CoolFolderList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from . import CoolDb

if TYPE_CHECKING:
    from . import CoolTui

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class CoolFolderList(urwid.WidgetWrap, AppPanel):
    """Widget class containing list COOL folders and foldersets.

    Parameters
    ----------
    app: `CoolTui`
        application class
    db: `CoolDb`
        instance of CoolDb class
    folderSet : `CoolDb.FolderSet`, optional
    """

    _selectable = True

    signals = ['folderSetSelected', 'folderSelected', 'showSchema']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CoolTui.CoolTui, db: CoolDb.CoolDb,
                 folderSet: CoolDb.FolderSet | None = None):
        self._app = app
        self._db = db
        self._folderSet = folderSet
        self._col_width = [1, -1, 2, -3]

        foldersets, folders = self._db.lsFolder(self._folderSet)

        self._total_count = len(foldersets) + len(folders)
        items = []
        for folderset in foldersets:
            item = UIColumns([UISelectableText("+"),
                              UISelectableText(folderset.name),
                              urwid.Text(""),
                              urwid.Text(folderset.description),
                              ],
                             self._col_width, 2, 0)
            urwid.connect_signal(item, 'activated', self._foldersetActivated, user_args=[folderset])
            item = urwid.AttrMap(item, "folderset-item", "folderset-selected")
            item.user_data = folderset
            items += [item]
        for folder in folders:
            item = UIColumns([UISelectableText(""),
                              UISelectableText(folder.name),
                              urwid.Text("MV" if folder.multi_version else ""),
                              urwid.Text(folder.description),
                              ],
                             self._col_width, 2, 0)
            urwid.connect_signal(item, 'activated', self._folderActivated, user_args=[folder])
            item = urwid.AttrMap(item, "folder-item", "folder-selected")
            # remember folder
            item.user_data = folder
            items += [item]

        header = UIColumns([urwid.Text('+'),
                            urwid.Text('Name'),
                            urwid.Text('MV'),
                            urwid.Text('Description'),
                            ],
                           self._col_width, 2)

        self._walker = urwid.SimpleFocusListWalker(items)
        lb = UIListBoxWithHeader(self._walker, header=header)
        self.__super.__init__(lb)

    def title(self) -> str:
        return "Folders in database " + self._db.name

    def hints(self) -> list[tuple[str, str]]:
        item = self._walker.get_focus()[0]
        folder = item.user_data

        hints = [('Enter', "Select")]
        if isinstance(folder, CoolDb.Folder):
            hints += [('^E', "Schema")]

        return hints

    def keypress(self, size: tuple[int, int], key: str) -> str | None:

        # handle '^E'
        if key == 'ctrl e':
            self._show_schema()
            return None

        # send it to base class
        key = self.__super.keypress(size, key)
        return key

    def _foldersetActivated(self, fs: CoolDb.FolderSet, item: urwid.Widget) -> None:
        _log.debug("emitting signal 'folderSetSelected': %r", fs)
        self._emit('folderSetSelected', self._db, fs)

    def _folderActivated(self, fs: CoolDb.Folder, item: urwid.Widget) -> None:
        _log.debug("emitting signal 'folderSelected': %r", fs)
        self._emit('folderSelected', self._db, fs)

    def _show_schema(self) -> None:
        # figure out which folder is selected
        item = self._walker.get_focus()[0]
        folder = item.user_data
        _log.debug("_show_schema folder: %r", folder)
        if isinstance(folder, CoolDb.Folder):
            _log.debug("emitting signal 'showSchema': %r", folder)
            self._emit('showSchema', self._db, folder)
