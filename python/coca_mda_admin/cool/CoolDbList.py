"""Module containing MdaMainMenu class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from .CoolDb import getDatabaseNames

if TYPE_CHECKING:
    from . import CoolTui

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class CoolDbList(UIListBoxWithHeader, AppPanel):
    """
    Widget class containing list of COOL databases.
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: CoolTui.CoolTui, conn_str: str):
        """
        @param app: instance of CoolTui application class
        @param conn_str:  CORAL connection string for database
        """

        self._app = app

        databases = getDatabaseNames(conn_str)

        col_width = [30]
        header = UIColumns(['COOL Databases'], col_width)

        items = []
        for dbName in databases:
            item = UIColumns([UISelectableText(dbName)], col_width, 0, 0)
            urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[dbName])
            items.append(item)

        UIListBoxWithHeader.__init__(self, items, header=header)

    def title(self) -> str:
        return 'COOL Database List'

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def _itemActivated(self, value: str, item: urwid.Widget) -> None:
        _log.debug("emitting signal 'selected': %r", value)
        self._emit('selected', value)
