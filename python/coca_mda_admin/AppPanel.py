"""Module for AppPanel class and related methods.
"""
from __future__ import annotations


class AppPanel:
    """Interface definition for application panels."""

    def title(self) -> str | None:
        """Return string displayed as a panel title"""
        raise NotImplementedError()

    def hints(self) -> list[tuple[str, str]]:
        """Return list of hints.

        Each hint is a tuple of two strings, first string is the name of
        a key and second is the name of the action, e.g. ("^E", "Menu").
        Default implementation returns empty list.
        """
        return []

    def status(self) -> str:
        """Return status string.

        Default implementation returns empty string.
        """
        return ""
