"""Module containing MdaFileList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import Any, TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from ..walkers import PageWalker

if TYPE_CHECKING:
    from . import MdaAdmin, MdaDb

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class MdaFileList(urwid.WidgetWrap, AppPanel):
    """Widget class containing list of MDA runs.

    Parameters
    ----------
    app : `MdaAdmin`
    db : `MdaDb`
    runlb_id: `int`
        ID of the RunLB record or None
    options: `dict`
        set of options passed to database API
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: MdaAdmin.MdaAdmin, db: MdaDb.MdaDb, runlb_id: int,
                 title: str = "", options: dict[str, Any] = {}):

        self._app = app
        self._db = db
        self._runlb_id = runlb_id
        self._title = title
        self._db_options = options.copy()
        self._col_width = [10, -3, -1, -4]
        self._total_count = self._counter()
        self._listwalker = PageWalker(self._pager, self._total_count)

        header = UIColumns([urwid.Text('ID', align='right'),
                            urwid.Text('File Name'),
                            urwid.Text('Dataset'),
                            urwid.Text('Archive')],
                           self._col_width, 2)

        lb = UIListBoxWithHeader(self._listwalker, header=header)
        self.__super.__init__(lb)

    def title(self) -> str:
        return self._title

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def status(self) -> str:
        status = "{} out of {} files".format(self._listwalker.focus + 1, self._total_count)
        return status

    def _itemActivated(self, file: MdaDb.File, item: UIColumns) -> None:
        _log.debug("emitting signal 'selected': %r %r", file.file_id, file.file_name)
        self._emit('selected', file.file_id, file.file_name)

    def _counter(self) -> int:
        stat = self._db.files_count(self._runlb_id, **self._db_options)
        return stat

    def _pager(self, offset: int, pageSize: int) -> list[urwid.Widget]:
        items = self._db.files(self._runlb_id, maxRows=pageSize, offset=offset, **self._db_options)
        return [self._widgetFactory(item) for item in items]

    def _widgetFactory(self, file: MdaDb.File) -> urwid.Widget:
        _log.debug("MdaFileList: widget factory: %r", file)
        item = UIColumns([UISelectableText(str(int(file.file_id)), align='right'),
                          urwid.Text(file.file_name),
                          urwid.Text(file.dataset),
                          urwid.Text(file.archive)],
                         self._col_width, 2, 0)

        urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[file])

        item = urwid.AttrMap(item, "list-item", "list-selected")
        return item
