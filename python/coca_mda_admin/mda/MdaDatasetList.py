"""Module containing MdaDatasetList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText

if TYPE_CHECKING:
    from . import MdaAdmin, MdaDb

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class MdaDatasetList(UIListBoxWithHeader, AppPanel):
    """Widget class containing list of CoCa datasets.

    Parameters
    ----------
    app : `MdaAdmin`
    db : `MdaDb`
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: MdaAdmin.MdaAdmin, db: MdaDb.MdaDb):
        # read data from database
        datasets = db.datasets()
        self._count = len(datasets)

        col_width = [10, -1]
        header = UIColumns([urwid.Text('ID', align='right'), 'Dataset Name'],
                           col_width, 2)

        items = []
        for ds in datasets:
            item = UIColumns([UISelectableText(str(int(ds.ds_id)), align='right'),
                              urwid.Text(ds.ds_name)],
                             col_width, 2, 0)
            urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[ds])
            items.append(item)

        UIListBoxWithHeader.__init__(self, items, header=header)

    def title(self) -> str:
        return "Datasets"

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def status(self) -> str:
        return "Found {} datasets".format(self._count)

    def _itemActivated(self, ds: MdaDb.Dataset, item: UIColumns) -> None:
        _log.debug("emitting signal 'selected': %r %r", ds.ds_id, ds.ds_name)
        self._emit('selected', ds.ds_id, ds.ds_name)
