"""Module containing MdaDb class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from collections import namedtuple, defaultdict
from collections.abc import Iterator
from contextlib import contextmanager
from datetime import datetime
from typing import NamedTuple, TypeVar, cast

#-----------------------------
# Imports for other modules --
#-----------------------------
from coldpie import coral

#----------------------------------
# Local non-exported definitions --
#----------------------------------
_log = logging.getLogger(__name__)

_NamedTupleType = TypeVar("_NamedTupleType", bound=NamedTuple)


def _ts2dt(ts: coral.TimeStamp) -> datetime:
    """Convert Coral timestamp into datetime"""
    return datetime(ts.year(), ts.month(), ts.day(),
                    ts.hour(), ts.minute(), ts.second(),
                    ts.nanosecond() // 1000)


def _row2nt(row: coral.AttributeList, tupletype: type[_NamedTupleType]) -> _NamedTupleType:
    """Covert Coral AttributeList (result row) into an named tuple."""
    kv = {}
    for attr in tupletype._fields:
        val = row[attr].data()
        if isinstance(val, coral.TimeStamp):
            val = _ts2dt(val)
        kv[attr] = val
    return tupletype(**kv)  # type: ignore[call-overload]


@contextmanager
def read_transaction(session: coral.ISessionProxy) -> Iterator:
    readonly = True
    trans = session.transaction()
    trans.start(readonly)
    yield trans
    trans.commit()


@contextmanager
def update_transaction(session: coral.ISessionProxy) -> Iterator:
    readonly = False
    trans = session.transaction()
    trans.start(readonly)
    yield trans
    trans.commit()

#------------------------
# Exported definitions --
#------------------------


Partition = namedtuple("Partition", """part_id part_name""")
Dataset = namedtuple("Dataset", """ds_id ds_name""")
LBSeq_internal = namedtuple("LBSeq_internal", """lbsq_id lbsq_size lbsq_min_lb lbsq_max_lb""")
LBSeq = namedtuple("LBSeq", """lbsq_id lbsq_size lbsq_min_lb lbsq_max_lb ranges""")
RunLB = namedtuple("RunLB", """runlb_id run_number lb_number sm_key""")
File = namedtuple("File", """file_id file_name dataset archive""")
Hgroup = namedtuple("Hgroup", """hgrp_id hgrp_name server provider""")
Histo = namedtuple("Histo", """hist_id folder histo_name lbsq_id""")


class MdaDb:
    """Interface for MDA database.
    """

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, conn_str: str, readonly: bool):
        """
        @param conn_str:  CORAL connection string for coca database
        @param readonly:  boolean, if True then use read-only mode for database
        """

        conn_svc = coral.ConnectionService()
        mode = coral.access_ReadOnly if readonly else coral.access_Update
        logging.debug("start new database session: %s", conn_str)
        self.session = conn_svc.connect(conn_str, mode)
        self.schema = self.session.nominalSchema()

        prefix = "MDA3_"
        self.partitionTbl = prefix + "PARTITION"
        self.runtypeTbl = prefix + "RUNTYPE"
        self.datasetTbl = prefix + "DATASET"
        self.archiveTbl = prefix + "ARCHIVE"
        self.providerTbl = prefix + "PROVIDER"
        self.hserverTbl = prefix + "HSERVER"
        self.hgrpnameTbl = prefix + "HGRPNAME"
        self.hnameTbl = prefix + "HNAME"
        self.runlbTbl = prefix + "RUNLB"
        self.fileTbl = prefix + "FILE"
        self.folderTbl = prefix + "FOLDER"
        self.histoTbl = prefix + "HISTO"
        self.hgroupTbl = prefix + "HGROUP"
        self.lbSeqTbl = prefix + "LBSEQ"
        self.lbRangeTbl = prefix + "LBRANGE"
        self.hgroup2histoTbl = prefix + "HGROUP_HISTO"

    def lbseq_count(self) -> int:
        """
        Returns total count of LB sequences defined
        """

        _log.debug('lbseq_count')

        with read_transaction(self.session):

            query = self.schema.tableHandle(self.lbSeqTbl).newQuery()
            query.addToOutputList("COUNT(*)")

            counter = 0
            for row in query.execute():
                counter = int(cast(float, row[0].data()))

        _log.debug('lbseq_count: found %r records', counter)
        return counter

    def lbseq(self, seq_id: int | None = None, maxRows: int | None = None,
              offset: int | None = None) -> list[LBSeq]:
        """
        Returns list if LBSeq instances ordered by ID

        @param seq_id: if nor None then only that ID is returned (or
                empty list if ID does not exist)
        @param maxRows: max. number of rows to return (used for paging)
        @param offset: starting position in number of rows (used for paging)
        """

        _log.debug('lbseq: %r maxrows=%r offset=%r', seq_id, maxRows, offset)

        with read_transaction(self.session):

            query = self.schema.tableHandle(self.lbSeqTbl).newQuery()
            attributes = 'lbsq_id lbsq_size lbsq_min_lb lbsq_max_lb'.split()
            for attr in attributes:
                query.addToOutputList(attr.upper(), attr)

            cdata = coral.AttributeList()
            conditions = []
            if seq_id is not None:
                idx = len(cdata)
                cdata.extend('LBSQ_ID', 'int')
                cdata[idx].setData(seq_id)
                conditions += ["LBSQ_ID = :LBSQ_ID"]

            if conditions:
                cond = " AND ".join(conditions)
                query.setCondition(cond, cdata)

            query.addToOrderList('LBSQ_ID')

            if maxRows is not None:
                if offset is None:
                    offset = 0
                query.limitReturnedRows(maxRows, offset)
                query.setRowCacheSize(maxRows)
            else:
                query.setRowCacheSize(100000)
            query.setMemoryCacheSize(10)

            lbseqs_internal = [_row2nt(row, LBSeq_internal) for row in query.execute()]

            range_map = defaultdict(lambda: [])
            if lbseqs_internal:
                # read sequences

                query = self.schema.tableHandle(self.lbRangeTbl).newQuery()
                attributes = 'lbsq_id lbrng_min lbrng_max'.split()
                for attr in attributes:
                    query.addToOutputList(attr.upper(), attr)

                if seq_id is not None or maxRows is not None:
                    min_id = min(lbseq.lbsq_id for lbseq in lbseqs_internal)
                    max_id = max(lbseq.lbsq_id for lbseq in lbseqs_internal)
                    cdata = coral.AttributeList()
                    cdata.extend('MIN', 'int')
                    cdata.extend('MAX', 'int')
                    cdata[0].setData(min_id)
                    cdata[1].setData(max_id)
                    cond = "LBSQ_ID >= :MIN AND LBSQ_ID <= :MAX"
                    query.setCondition(cond, cdata)
                    _log.debug('lbseq: ranges %r %r', min_id, max_id)

                query.setRowCacheSize(100000)
                query.setMemoryCacheSize(10)

                for row in query.execute():
                    lbseq_id = row[0].data()
                    lb_range = row[1].data(), row[2].data()
                    range_map[lbseq_id].append(lb_range)

            lbseqs = [
                LBSeq(lbsq_id=lbs.lbsq_id, lbsq_size=lbs.lbsq_size, lbsq_min_lb=lbs.lbsq_min_lb,
                      lbsq_max_lb=lbs.lbsq_max_lb, ranges=range_map[lbs.lbsq_id])
                for lbs in lbseqs_internal
            ]

        _log.debug('lbseq: found %r records', len(lbseqs))
        return lbseqs

    def partitions(self) -> list[Partition]:
        """
        Returns list of Partition instances ordered by partition name
        """

        _log.debug('partitions')

        with read_transaction(self.session):

            query = self.schema.tableHandle(self.partitionTbl).newQuery()
            attributes = 'part_id part_name'.split()
            for attr in attributes:
                query.addToOutputList(attr.upper(), attr)

            query.addToOrderList('PART_NAME')

            partitions = [_row2nt(row, Partition) for row in query.execute()]

        _log.debug('partitions: found %r records', len(partitions))
        return partitions

    def datasets(self) -> list[Dataset]:
        """
        Returns list of Dataset instances ordered by dataset name
        """

        _log.debug('datasets')

        with read_transaction(self.session):

            query = self.schema.tableHandle(self.datasetTbl).newQuery()
            attributes = 'ds_id ds_name'.split()
            for attr in attributes:
                query.addToOutputList(attr.upper(), attr)

            query.addToOrderList('DS_NAME')

            items = [_row2nt(row, Dataset) for row in query.execute()]

        _log.debug('datasets: found %r records', len(items))
        return items

    def runs_count(self, part_id: int | None) -> int:
        """
        Returns number of RunLB record for given partition (or all partitions)

        @param part_id:  Partition ID or None
        @param maxRows: max. number of rows to return (used for paging)
        @param offset: starting position in number of rows (used for paging)
        """

        _log.debug('runs_count')

        with read_transaction(self.session):

            query = self.schema.tableHandle(self.runlbTbl).newQuery()
            query.addToOutputList("COUNT(*)", "n_runs")

            cdata = coral.AttributeList()
            cond = ""
            if part_id is not None:
                idx = len(cdata)
                cdata.extend('PART_ID', 'int')
                cdata[idx].setData(part_id)
                cond = "PART_ID = :PART_ID"
            if cond:
                _log.debug('archives_count: cond: %r', cond)
                query.setCondition(cond, cdata)

            counter = 0
            for row in query.execute():

                counter = int(cast(float, row[0].data()))

        return counter

    def runs(self, part_id: int | None, maxRows: int | None = None,
             offset: int | None = None) -> list[RunLB]:
        """
        Returns list of RunLB instances ordered by runLB ID in descending order

        @param part_id:  Partition ID or None
        @param maxRows: max. number of rows to return (used for paging)
        @param offset: starting position in number of rows (used for paging)
        """

        _log.debug('runs')

        with read_transaction(self.session):

            query = self.schema.tableHandle(self.runlbTbl).newQuery()
            attributes = 'runlb_id run_number lb_number sm_key'.split()
            for attr in attributes:
                query.addToOutputList(attr.upper(), attr)

            cdata = coral.AttributeList()
            cond = ""
            if part_id is not None:
                idx = len(cdata)
                cdata.extend('PART_ID', 'int')
                cdata[idx].setData(part_id)
                cond = "PART_ID = :PART_ID"
            if cond:
                _log.debug('archives_count: cond: %r', cond)
                query.setCondition(cond, cdata)

            query.addToOrderList('RUNLB_ID DESC')

            if maxRows is not None:
                if offset is None:
                    offset = 0
                query.limitReturnedRows(maxRows, offset)
                query.setRowCacheSize(maxRows)
            else:
                query.setRowCacheSize(100000)
            query.setMemoryCacheSize(10)

            runs = [_row2nt(row, RunLB) for row in query.execute()]

        _log.debug('runs: found %r records', len(runs))
        return runs

    def files_count(self, runlb_id: int | None, archived: bool | None = None,
                    ds_id: int | None = None) -> int:
        """
        Returns number of File records for a given RunLB

        @param runlb_id:  RunLB ID or None
        """

        _log.debug('files_count')

        with read_transaction(self.session):

            query = self.schema.newQuery()
            query.addToTableList(self.fileTbl, "FI")
            query.addToOutputList("COUNT(*)", "n_items")

            cdata = coral.AttributeList()
            conditions = []
            if runlb_id is not None:
                idx = len(cdata)
                cdata.extend('RUNLB_ID', 'int')
                cdata[idx].setData(runlb_id)
                conditions += ["FI.RUNLB_ID = :RUNLB_ID"]
            if archived is not None:
                query.addToTableList(self.archiveTbl, "AR")
                conditions += ["FI.ARCH_ID = AR.ARCH_ID"]
                idx = len(cdata)
                cdata.extend('AR_NAME', 'string')
                cdata[idx].setData('_FILE_NOT_REGISTERED_')
                if archived:
                    conditions += ["AR.ARCH_NAME <> :AR_NAME"]
                else:
                    conditions += ["AR.ARCH_NAME = :AR_NAME"]
            if ds_id is not None:
                idx = len(cdata)
                cdata.extend('DS_ID', 'int')
                cdata[idx].setData(ds_id)
                conditions += ["FI.DS_ID = :DS_ID"]
            if conditions:
                _log.debug('files_count: conditions: %r', conditions)
                cond = " AND ".join(conditions)
                query.setCondition(cond, cdata)

            counter = 0
            for row in query.execute():
                counter = int(cast(float, row[0].data()))

        return counter

    def files(self, runlb_id: int | None, archived: bool | None = None, ds_id: int | None = None,
              maxRows: int | None = None, offset: int | None = None) -> list[File]:
        """
        Returns list of File instances ordered by file_id

        @param runlb_id:  Partition ID or None
        @param maxRows: max. number of rows to return (used for paging)
        @param offset: starting position in number of rows (used for paging)
        """

        _log.debug('files')

        with read_transaction(self.session):

            query = self.schema.newQuery()
            query.addToTableList(self.fileTbl, "FI")
            query.addToTableList(self.datasetTbl, "DS")
            query.addToTableList(self.archiveTbl, "AR")

            query.addToOutputList('FI.FILE_ID', 'file_id')
            query.addToOutputList('FI.FILE_NAME', 'file_name')
            query.addToOutputList('DS.DS_NAME', 'dataset')
            query.addToOutputList('AR.ARCH_NAME', 'archive')

            cdata = coral.AttributeList()
            conditions = ["FI.DS_ID = DS.DS_ID",
                          "FI.ARCH_ID = AR.ARCH_ID"]
            if runlb_id is not None:
                idx = len(cdata)
                cdata.extend('RUNLB_ID', 'int')
                cdata[idx].setData(runlb_id)
                conditions += ["FI.RUNLB_ID = :RUNLB_ID"]
            if archived is not None:
                idx = len(cdata)
                cdata.extend('AR_NAME', 'string')
                cdata[idx].setData('_FILE_NOT_REGISTERED_')
                if archived:
                    conditions += ["AR.ARCH_NAME <> :AR_NAME"]
                else:
                    conditions += ["AR.ARCH_NAME = :AR_NAME"]
            if ds_id is not None:
                idx = len(cdata)
                cdata.extend('DS_ID', 'int')
                cdata[idx].setData(ds_id)
                conditions += ["FI.DS_ID = :DS_ID"]

            cond = " AND ".join(conditions)
            query.setCondition(cond, cdata)

            query.addToOrderList('FI.FILE_ID')

            if maxRows is not None:
                if offset is None:
                    offset = 0
                query.limitReturnedRows(maxRows, offset)
                query.setRowCacheSize(maxRows)
            else:
                query.setRowCacheSize(100000)
            query.setMemoryCacheSize(10)

            files = [_row2nt(row, File) for row in query.execute()]

        return files

    def hgrps_count(self, file_id: int | None) -> int:
        """
        Returns number of HGRP instances for a given file

        @param file_id:  FILE ID or None
        """

        _log.debug('hgrps_count')

        with read_transaction(self.session):

            query = self.schema.tableHandle(self.hgroupTbl).newQuery()
            query.addToOutputList("COUNT(*)", "n_items")

            cdata = coral.AttributeList()
            cond = ""
            if file_id is not None:
                idx = len(cdata)
                cdata.extend('FILE_ID', 'int')
                cdata[idx].setData(file_id)
                cond = "FILE_ID = :FILE_ID"
            if cond:
                _log.debug('hgrps_count: cond: %r', cond)
                query.setCondition(cond, cdata)

            counter = 0
            for row in query.execute():
                counter = int(cast(float, row[0].data()))

        return counter

    def hgrps(self, file_id: int | None, maxRows: int | None = None,
              offset: int | None = None) -> list[Hgroup]:
        """
        Returns list of Hgroup instances ordered by hgrp_id

        @param file_id:  FILE ID or None
        @param maxRows: max. number of rows to return (used for paging)
        @param offset: starting position in number of rows (used for paging)
        """

        _log.debug('hgrps')

        with read_transaction(self.session):

            query = self.schema.newQuery()
            query.addToTableList(self.hgroupTbl, "HG")
            query.addToTableList(self.hgrpnameTbl, "HGNM")
            query.addToTableList(self.hserverTbl, "SRV")
            query.addToTableList(self.providerTbl, "PROV")

            query.addToOutputList('HG.HGRP_ID', 'hgrp_id')
            query.addToOutputList('HGNM.HGRPNAME_NAME', 'hgrp_name')
            query.addToOutputList('SRV.HSRV_NAME', 'server')
            query.addToOutputList('PROV.PROV_NAME', 'provider')

            cdata = coral.AttributeList()
            conditions = ["HG.HGRPNAME_ID = HGNM.HGRPNAME_ID",
                          "HG.HSRV_ID = SRV.HSRV_ID",
                          "HG.PROV_ID = PROV.PROV_ID"]
            if file_id is not None:
                idx = len(cdata)
                cdata.extend('FILE_ID', 'int')
                cdata[idx].setData(file_id)
                conditions += ["HG.FILE_ID = :FILE_ID"]

            cond = " AND ".join(conditions)
            query.setCondition(cond, cdata)

            query.addToOrderList('HG.HGRP_ID')

            if maxRows is not None:
                if offset is None:
                    offset = 0
                query.limitReturnedRows(maxRows, offset)
                query.setRowCacheSize(maxRows)
            else:
                query.setRowCacheSize(100000)
            query.setMemoryCacheSize(10)

            items = [_row2nt(row, Hgroup) for row in query.execute()]

        return items

    def histo_count(self, hgrp_id: int | None) -> int:
        """
        Returns number of HISTO instances for a given HGRP

        @param hgrp_id:  HGRP ID or None
        """

        _log.debug('histo_count')

        with read_transaction(self.session):

            query = self.schema.tableHandle(self.hgroup2histoTbl).newQuery()
            query.addToOutputList("COUNT(*)", "n_items")

            cdata = coral.AttributeList()
            cond = ""
            if hgrp_id is not None:
                idx = len(cdata)
                cdata.extend('HGRP_ID', 'int')
                cdata[idx].setData(hgrp_id)
                cond = "HGRP_ID = :HGRP_ID"
            if cond:
                _log.debug('histo_count: cond: %r', cond)
                query.setCondition(cond, cdata)

            counter = 0
            for row in query.execute():
                counter = int(cast(float, row[0].data()))

        return counter

    def histos(self, hgrp_id: int | None, maxRows: int | None = None,
               offset: int | None = None) -> list[Histo]:
        """
        Returns list of Histo instances ordered by forlder name and histogram name

        @param hgrp_id:  HGRP ID or None
        @param maxRows: max. number of rows to return (used for paging)
        @param offset: starting position in number of rows (used for paging)
        """

        _log.debug('histos')

        with read_transaction(self.session):

            query = self.schema.newQuery()
            query.addToTableList(self.hgroup2histoTbl, "H2H")
            query.addToTableList(self.histoTbl, "HI")
            query.addToTableList(self.hnameTbl, "HNM")
            query.addToTableList(self.folderTbl, "FNM")

            query.addToOutputList('HI.HIST_ID', 'hist_id')
            query.addToOutputList('FNM.FLDR_NAME', 'folder')
            query.addToOutputList('HNM.HNAM_NAME', 'histo_name')
            query.addToOutputList('H2H.LBSQ_ID', 'lbsq_id')

            cdata = coral.AttributeList()
            conditions = ["H2H.HIST_ID = HI.HIST_ID",
                          "HI.FLDR_ID = FNM.FLDR_ID",
                          "HI.HNAM_ID = HNM.HNAM_ID"]
            if hgrp_id is not None:
                idx = len(cdata)
                cdata.extend('HGRP_ID', 'int')
                cdata[idx].setData(hgrp_id)
                conditions += ["H2H.HGRP_ID = :HGRP_ID"]

            cond = " AND ".join(conditions)
            query.setCondition(cond, cdata)

            query.addToOrderList('FNM.FLDR_NAME')
            query.addToOrderList('HNM.HNAM_NAME')

            query.setDistinct()

            if maxRows is not None:
                if offset is None:
                    offset = 0
                query.limitReturnedRows(maxRows, offset)
                query.setRowCacheSize(maxRows)
            else:
                query.setRowCacheSize(100000)
            query.setMemoryCacheSize(10)

            items = [_row2nt(row, Histo) for row in query.execute()]

        return items
