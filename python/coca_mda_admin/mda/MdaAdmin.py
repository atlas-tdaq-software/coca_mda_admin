"""Module for CocaAdmin class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import Any

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppBase import AppBase
from .MdaDatasetList import MdaDatasetList
from .MdaDb import MdaDb
from .MdaFileList import MdaFileList
from .MdaHgroupList import MdaHgroupList
from .MdaHistoList import MdaHistoList
from .MdaLBSeqList import MdaLBSeqList
from .MdaMainMenu import MdaMainMenu
from .MdaPartitionList import MdaPartitionList
from .MdaRunList import MdaRunList

#----------------------------------
# Local non-exported definitions --
#----------------------------------
_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class MdaAdmin(AppBase):
    """Class representing top-level widget for the whole application.

    Parameters
    ----------
    conn_str : `str`
        CORAL connection string for mda database
    readonly : `str`
        boolean, if True then use read-only mode for database
    """

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, conn_str: str, readonly: bool):
        AppBase.__init__(self)
        self.db = MdaDb(conn_str, readonly)

    def title(self) -> str:
        return ' = MDA database administration = '

    def start(self, loop: urwid.MainLoop, user_data: Any) -> None:
        """Make initial panel
        """
        mdaMenu = self.makePanel(MdaMainMenu)
        urwid.connect_signal(mdaMenu, 'selected', self.mainMenuSelected)

    def mainMenuSelected(self, widget: urwid.Widget, mainMenuItem: str) -> None:
        _log.debug("selected item: %r", mainMenuItem)

        if mainMenuItem == 'partitions':
            widget = self.makePanel(MdaPartitionList, self.db)
            urwid.connect_signal(widget, 'selected', self.partitionSelected)
        elif mainMenuItem == 'datasets':
            widget = self.makePanel(MdaDatasetList, self.db)
            urwid.connect_signal(widget, 'selected', self.datasetSelected)
        elif mainMenuItem == 'files_not_archived':
            title = "Files not in CoCa"
            options = dict(archived=False)
            widget = self.makePanel(MdaFileList, self.db, None, title, options)
            urwid.connect_signal(widget, 'selected', self.fileSelected)
        elif mainMenuItem == 'lb_seq':
            widget = self.makePanel(MdaLBSeqList, self.db)

    def partitionSelected(self, widget: urwid.Widget, part_id: int, partition: str) -> None:
        _log.debug("selected item: %r", (part_id, partition))
        widget = self.makePanel(MdaRunList, self.db, part_id, partition)
        urwid.connect_signal(widget, 'selected', self.runLbSelected)

    def datasetSelected(self, widget: urwid.Widget, ds_id: int, ds_name: str) -> None:
        _log.debug("selected item: %r", (ds_id, ds_name))
        options = dict(ds_id=ds_id)
        title = "File list for dataset %r" % ds_name
        widget = self.makePanel(MdaFileList, self.db, None, title, options)
        urwid.connect_signal(widget, 'selected', self.fileSelected)

    def runLbSelected(self, widget: urwid.Widget, runlb_id: int, run: int, lb: int) -> None:
        _log.debug("selected item: %r %r %r", runlb_id, run, lb)
        if runlb_id is None:
            title = "File list for all runs"
        else:
            title = "File list for run {}".format(run)
            if lb is None:
                title += " (EoR)"
            else:
                title += " (LB={})".format(lb)
        widget = self.makePanel(MdaFileList, self.db, runlb_id, title)
        urwid.connect_signal(widget, 'selected', self.fileSelected)

    def fileSelected(self, widget: urwid.Widget, file_id: int, file_name: str) -> None:
        _log.debug("selected item: %r", file_id)
        widget = self.makePanel(MdaHgroupList, self.db, file_id)
        urwid.connect_signal(widget, 'selected', self.hgroupSelected)

    def hgroupSelected(self, widget: urwid.Widget, hgrp_id: int) -> None:
        _log.debug("selected item: %r", hgrp_id)
        widget = self.makePanel(MdaHistoList, self.db, hgrp_id)
        urwid.connect_signal(widget, 'selected', self.histoSelected)

    def histoSelected(self, widget: urwid.Widget, hist_id: int, lbsq_id: int) -> None:
        _log.debug("selected item: %r %r", hist_id, lbsq_id)
        if lbsq_id is None:
            _log.debug("LBSQ_ID is None, returning")
            return
        self.makePanel(MdaLBSeqList, self.db, lbsq_id)
