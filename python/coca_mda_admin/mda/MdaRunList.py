"""Module containing MdaRunList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from ..walkers import PageWalker

if TYPE_CHECKING:
    from . import MdaAdmin, MdaDb

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class MdaRunList(urwid.WidgetWrap, AppPanel):
    """Widget class containing list of MDA runs.

    Parameters
    ----------
    app : `MdaAdmin`
    db : `MdaDb`
    part_id : `int`
        ID of the partition
    partition : `str`
        name of the partition
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: MdaAdmin.MdaAdmin, db: MdaDb.MdaDb, part_id: int, partition: str):

        self._app = app
        self._db = db
        self._part_id = part_id
        self._col_width = [10, 10, 10, 10]
        self._total_count = self._counter()
        self._listwalker = PageWalker(self._pager, self._total_count)

        header = UIColumns([urwid.Text('ID', align='right'),
                            urwid.Text('Run', align='right'),
                            urwid.Text('LB', align='right'),
                            urwid.Text('SM Key', align='right')],
                           self._col_width, 2)

        lb = UIListBoxWithHeader(self._listwalker, header=header)
        self.__super.__init__(lb)

    def title(self) -> str:
        if self._part_id is None:
            return "Run list for all partitions"
        else:
            return "Run list for partition {!r}".format(self._part_id)

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def status(self) -> str:
        status = "{} out of {} Run-LB entries".format(self._listwalker.focus + 1, self._total_count)
        return status

    def keypress(self, size: tuple, key: str) -> str | None:
        # send it to base class
        key = self.__super.keypress(size, key)
        return key

    def _itemActivated(self, runlb: MdaDb.RunLB, item: UIColumns) -> None:
        _log.debug("emitting signal 'selected': %r %r %r", runlb.runlb_id, runlb.run_number, runlb.lb_number)
        self._emit('selected', runlb.runlb_id, runlb.run_number, runlb.lb_number)

    def _counter(self) -> int:
        stat = self._db.runs_count(self._part_id)
        return stat

    def _pager(self, offset: int, pageSize: int) -> list[urwid.Widget]:
        items = self._db.runs(self._part_id, maxRows=pageSize, offset=offset)
        return [self._widgetFactory(item) for item in items]

    def _widgetFactory(self, runlb: MdaDb.RunLB) -> urwid.Widget:
        _log.debug("MdaRunList: widget factory: %r", runlb)
        lb_number = runlb.lb_number
        if lb_number is None:
            lb_number = "EoR"
        else:
            lb_number = str(int(lb_number))
        sm_key = runlb.sm_key
        if sm_key is None:
            sm_key = "None"
        else:
            sm_key = str(int(sm_key))
        item = UIColumns([UISelectableText(str(int(runlb.runlb_id)), align='right'),
                          urwid.Text(str(int(runlb.run_number)), align='right'),
                          urwid.Text(lb_number, align='right'),
                          urwid.Text(sm_key, align='right')],
                         self._col_width, 2, 0)

        urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[runlb])

        item = urwid.AttrMap(item, "list-item", "list-selected")
        return item
