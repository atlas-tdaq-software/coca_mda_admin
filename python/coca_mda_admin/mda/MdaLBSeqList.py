"""Module containing MdaLBSeqList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from ..walkers import PageWalker

if TYPE_CHECKING:
    from . import MdaAdmin, MdaDb

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)


def _formatRanges(min_lb: int, max_lb: int, total: int, ranges: list[tuple[int, int]]) -> str:

    dist = max_lb + 1 - min_lb
    if len(ranges) > 1 and (dist - total) < (dist // 10):
        res = str(min_lb) + '-' + str(max_lb)
        whole_range = set(range(min_lb, max_lb + 1))
        for min_lb, max_lb in ranges:
            whole_range -= set(range(min_lb, max_lb + 1))
        exclude = ' '.join(str(x) for x in sorted(whole_range))
        res += ' (except: ' + exclude + ')'
        return res
    else:
        rlist = []
        for min_lb, max_lb in ranges:
            if min_lb == max_lb:
                rlist += [str(min_lb)]
            else:
                rlist += [str(min_lb) + '-' + str(max_lb)]
        return ' '.join(rlist)

#------------------------
# Exported definitions --
#------------------------


class MdaLBSeqList(urwid.WidgetWrap, AppPanel):
    """Widget class containing list of CoCa datasets.

    Parameters
    ----------
    app : `MdaAdmin`
    db : `MdaDb`
    lbsq_id : `int`
        Id oof LBSQ to display, None to display all
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: MdaAdmin.MdaAdmin, db: MdaDb.MdaDb, lbsq_id: int | None = None):

        self._app = app
        self._db = db
        self._lbsq_id = lbsq_id
        self._col_width = [8, 6, 6, 6, -1]
        self._total_count = self._counter()
        self._listwalker = PageWalker(self._pager, self._total_count)

        header = UIColumns([urwid.Text('ID', align='right'),
                            urwid.Text('SeqLen', align='right'),
                            urwid.Text('MinLB', align='right'),
                            urwid.Text('MaxLB', align='right'),
                            urwid.Text('Lumi Blocks')], self._col_width, 1)

        lb = UIListBoxWithHeader(self._listwalker, header=header)
        self.__super.__init__(lb)

    def title(self) -> str:
        if self._lbsq_id is None:
            return "LB Sequences"
        else:
            return "LB Sequences for LBSQ_ID={}".format(self._lbsq_id)

    def status(self) -> str:
        status = "{} out of {} sequences".format(self._listwalker.focus + 1, self._total_count)
        return status

    def _counter(self) -> int:
        return self._db.lbseq_count()

    def _pager(self, offset: int, pageSize: int) -> list[urwid.Widget]:
        items = self._db.lbseq(self._lbsq_id, maxRows=pageSize, offset=offset)
        return [self._widgetFactory(item) for item in items]

    def _widgetFactory(self, lbseq: MdaDb.LBSeq) -> urwid.Widget:

        ranges = _formatRanges(lbseq.lbsq_min_lb, lbseq.lbsq_max_lb, lbseq.lbsq_size, lbseq.ranges)

        item = UIColumns([UISelectableText(str(int(lbseq.lbsq_id)), align='right'),
                          urwid.Text(str(int(lbseq.lbsq_size)), align='right'),
                          urwid.Text(str(int(lbseq.lbsq_min_lb)), align='right'),
                          urwid.Text(str(int(lbseq.lbsq_max_lb)), align='right'),
                          urwid.Text(ranges)],
                         self._col_width, 1, 0)
        item = urwid.AttrMap(item, "list-item", "list-selected")
        return item
