"""Module containing MdaPartitionList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from . import MdaDb


if TYPE_CHECKING:
    from . import MdaAdmin

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class MdaPartitionList(UIListBoxWithHeader, AppPanel):
    """Widget class containing list of CoCa datasets.

    Parameters
    ----------
    app : `MdaAdmin`
    db : `MdaDb`
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: MdaAdmin.MdaAdmin, db: MdaDb.MdaDb):

        # read data from database
        partitions = db.partitions()
        self._count = len(partitions)
        if partitions:
            partitions.insert(0, MdaDb.Partition(part_id=None, part_name=None))

        col_width = [10, -1]
        header = UIColumns([urwid.Text('ID', align='right'),
                            'Partition'],
                           col_width, 2)

        items = []
        for part in partitions:
            part_id = part.part_id
            if part_id is None:
                part_id = ""
                part_name = "<ALL PARTITIONS>"
            else:
                part_id = str(int(part_id))
                part_name = part.part_name
            item = UIColumns([UISelectableText(part_id, align='right'),
                              urwid.Text(part_name)],
                             col_width, 2, 0)
            urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[part])
            items.append(item)

        UIListBoxWithHeader.__init__(self, items, header=header)

    def title(self) -> str:
        return "Partitions"

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def status(self) -> str:
        return "Found {} partitions".format(self._count)

    def _itemActivated(self, part: MdaDb.Partition, item: UIColumns) -> None:
        _log.debug("emitting signal 'selected': %r %r", part.part_id, part.part_name)
        self._emit('selected', part.part_id, part.part_name)
