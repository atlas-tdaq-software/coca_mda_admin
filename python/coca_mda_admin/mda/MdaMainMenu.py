"""Module containing MdaMainMenu class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText

if TYPE_CHECKING:
    from . import MdaAdmin

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class MdaMainMenu(UIListBoxWithHeader, AppPanel):
    """Widget class containing list of possible options in MDA main menu.

    Parameters
    ----------
    app : `MdaAdmin`
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: MdaAdmin.MdaAdmin):
        self._app = app

        menu = [("Paritions Table", "partitions"),
                ("Datasets table", "datasets"),
                ("Files not in CoCa", "files_not_archived"),
                ("LB Sequences Table", "lb_seq")]

        col_width = [30]
        header = UIColumns(['Data selection'], col_width, 2)

        items = []
        for selection, value in menu:
            item = UIColumns([UISelectableText(selection)], col_width, 2, 0)
            urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[value])
            items.append(item)

        UIListBoxWithHeader.__init__(self, items, header=header)

    def title(self) -> str:
        return 'Information to browse'

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def status(self) -> str:
        return "Database information selector"

    def _itemActivated(self, value: str, item: UIColumns) -> None:
        _log.debug("emitting signal 'selected': %r", value)
        self._emit('selected', value)
