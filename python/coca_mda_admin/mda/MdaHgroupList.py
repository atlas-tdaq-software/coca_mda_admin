"""Module containing MdaHgroupList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from ..walkers import PageWalker

if TYPE_CHECKING:
    from . import MdaAdmin, MdaDb

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class MdaHgroupList(urwid.WidgetWrap, AppPanel):
    """Widget class containing list of MDA runs.

    Parameters
    ----------
    app : `MdaAdmin`
    db: `MdaDb`
    file_id : `int`
        ID of the file record
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: MdaAdmin.MdaAdmin, db: MdaDb.MdaDb, file_id: int):
        self._app = app
        self._db = db
        self._file_id = file_id
        self._col_width = [10, -1, -1, -1]
        self._total_count = self._counter()
        self._listwalker = PageWalker(self._pager, self._total_count)

        header = UIColumns([urwid.Text('ID', align='right'),
                            urwid.Text('Group Name'),
                            urwid.Text('server Name'),
                            urwid.Text('Provider Name')],
                           self._col_width, 2)

        lb = UIListBoxWithHeader(self._listwalker, header=header)
        self.__super.__init__(lb)

    def title(self) -> str:
        if self._file_id is None:
            return "Histo groups for all files"
        else:
            return "Histo groups for FILE_ID={!r}".format(self._file_id)

    def hints(self) -> list[tuple[str, str]]:
        return [('Enter', "Select")]

    def status(self) -> str:
        status = "{} out of {} hgroup entries".format(self._listwalker.focus + 1, self._total_count)
        return status

    def _itemActivated(self, hgrp: MdaDb.Hgroup, item: UIColumns) -> None:
        _log.debug("emitting signal 'selected': %r", hgrp.hgrp_id)
        self._emit('selected', hgrp.hgrp_id)

    def _counter(self) -> int:
        stat = self._db.hgrps_count(self._file_id)
        return stat

    def _pager(self, offset: int, pageSize: int) -> list[urwid.Widget]:
        items = self._db.hgrps(self._file_id, maxRows=pageSize, offset=offset)
        return [self._widgetFactory(item) for item in items]

    def _widgetFactory(self, hgrp: MdaDb.Hgroup) -> urwid.Widget:
        _log.debug("MdaHgroupList: widget factory: %r", hgrp)
        item = UIColumns([UISelectableText(str(int(hgrp.hgrp_id)), align='right'),
                          urwid.Text(hgrp.hgrp_name),
                          urwid.Text(hgrp.server),
                          urwid.Text(hgrp.provider)],
                         self._col_width, 2, 0)

        urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[hgrp])

        item = urwid.AttrMap(item, "list-item", "list-selected")
        return item
