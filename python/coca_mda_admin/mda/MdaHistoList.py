"""Module containing MdaHistoList class and related methods.
"""
from __future__ import annotations

#--------------------------------
#  Imports of standard modules --
#--------------------------------
import logging
from typing import TYPE_CHECKING

#-----------------------------
# Imports for other modules --
#-----------------------------
import urwid
from ..AppPanel import AppPanel
from ..ui import UIListBoxWithHeader, UIColumns, UISelectableText
from ..walkers import PageWalker

if TYPE_CHECKING:
    from . import MdaAdmin, MdaDb

#----------------------------------
# Local non-exported definitions --
#----------------------------------

_log = logging.getLogger(__name__)

#------------------------
# Exported definitions --
#------------------------


class MdaHistoList(urwid.WidgetWrap, AppPanel):
    """Widget class containing list of MDA runs.

    Parameters
    ----------
    app : `MdaAdmin`
    db : `MdaDb`
    hgrp_id : `int`
        ID of the HGRP record
    """

    _selectable = True

    signals = ['selected']

    #----------------
    #  Constructor --
    #----------------
    def __init__(self, app: MdaAdmin.MdaAdmin, db: MdaDb.MdaDb, hgrp_id: int):
        self._app = app
        self._db = db
        self._hgrp_id = hgrp_id
        self._col_width = [10, 8, -3, -2]
        self._total_count = self._counter()
        self._listwalker = PageWalker(self._pager, self._total_count)

        header = UIColumns([urwid.Text('ID', align='right'),
                            urwid.Text('LBSQ', align='right'),
                            urwid.Text('Folder Name'),
                            urwid.Text('Histogram Name')],
                           self._col_width, 2)

        lb = UIListBoxWithHeader(self._listwalker, header=header)
        self.__super.__init__(lb)

    def title(self) -> str:
        if self._hgrp_id is None:
            return "Histograms for all groups"
        else:
            return "Histograms for group {!r}".format(self._hgrp_id)

    def hints(self) -> list[tuple[str, str]]:
        item = self._listwalker.get_focus()[0]
        assert item is not None
        histo = item.user_data
        if histo.lbsq_id is not None:
            return [('Enter', "ShowLB")]
        else:
            return []

    def status(self) -> str:
        status = "{} out of {} histograms".format(self._listwalker.focus + 1, self._total_count)
        return status

    def keypress(self, size: tuple, key: str) -> str | None:
        # send it to base class
        key = self.__super.keypress(size, key)
        return key

    def _itemActivated(self, histo: MdaDb.Histo, item: UIColumns) -> None:
        _log.debug("emitting signal 'selected': %r %r", histo.hist_id, histo.lbsq_id)
        self._emit('selected', histo.hist_id, histo.lbsq_id)

    def _counter(self) -> int:
        stat = self._db.histo_count(self._hgrp_id)
        return stat

    def _pager(self, offset: int, pageSize: int) -> list[urwid.Widget]:
        items = self._db.histos(self._hgrp_id, maxRows=pageSize, offset=offset)
        return [self._widgetFactory(item) for item in items]

    def _widgetFactory(self, histo: MdaDb.Histo) -> urwid.Widget:
        _log.debug("MdaHistoList: widget factory: %r", histo)
        lbsq = "-" if histo.lbsq_id is None else str(int(histo.lbsq_id))
        item = UIColumns([UISelectableText(str(int(histo.hist_id)), align='right'),
                          urwid.Text(lbsq, align='right'),
                          urwid.Text(histo.folder),
                          urwid.Text(histo.histo_name)],
                         self._col_width, 2, 0)

        urwid.connect_signal(item, 'activated', self._itemActivated, user_args=[histo])

        item = urwid.AttrMap(item, "list-item", "list-selected")
        item.user_data = histo
        return item
