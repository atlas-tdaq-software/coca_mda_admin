This repository contains tools and utilities to manage content of the coca
and databases.

This tools do not use regular C++ coca API but instead implement few low-level
operations using direct database access via CORAL API. The package is supposed
to be pure Python and contain both libraries , command-line utilities, and 
GUIs (or TUIs) to analyze and modify coca/mda databases.